﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GamePreloading : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        ChatConfig chatConfig = Resources.Load<ChatConfig>("ChatConfig");
        
        if (chatConfig.isClient)
        {
            if (chatConfig.testClient)
            {
                SceneManager.LoadScene("TestClientChat");
            }
            else
            {
                SceneManager.LoadScene("GameScene");    
            }
        }
        else
        {
            SceneManager.LoadScene("ServerChat");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
