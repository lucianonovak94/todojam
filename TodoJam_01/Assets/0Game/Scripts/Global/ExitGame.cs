﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Button = UnityEngine.UI.Button;

public class ExitGame : MonoBehaviour
{
    [SerializeField, Required]
    private Button _btnAccept;

    void Awake()
    {
        _btnAccept.onClick.AddListener(onExitClick);
    }

    // Update is called once per frame
    void onExitClick()
    {
#if UNITY_EDITOR
         UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
    }
}
