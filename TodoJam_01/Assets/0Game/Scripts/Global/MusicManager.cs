﻿using System.Collections;
using System.Collections.Generic;
using UltimaCookie.Framework.Core;
using UnityEngine;

public class MusicManager : UCMonoSingleton<MusicManager>
{
    public AudioClip _clipBGMusic;
    public AudioClip _clipFightMusic;
    public AudioClip _clipMasterChatMusic;
    
    public AudioClip _clipVictory;

    public AudioSource ac;
    
    
    void Start()
    {
        ac.loop = true;
        ac.spatialize = false;
        ac.spatialBlend = 1f;
        
        PlayBGMusic();
    }

    public void PlayBGMusic()
    {
        if(ac.clip == _clipBGMusic)
            return;
        
        ac.loop = true;
        ac.clip = _clipBGMusic;
        AudioListener.volume = 1.5f;
        
        ac.Play();
    }

    public void PlayFightMusic()
    {
        if(ac.clip == _clipFightMusic)
            return;
        
        ac.loop = true;
        ac.clip = _clipFightMusic;
        AudioListener.volume = 0.1f;
        ac.Play();
    }

    public void PlayMasterChatMusic()
    {
        if(ac.clip == _clipMasterChatMusic)
            return;
        
        ac.loop = true;
        ac.clip = _clipMasterChatMusic;
        AudioListener.volume = 0.1f;
        ac.Play();
    }
    
    public void PlayVictoryMusic()
    {
        if(ac.clip == _clipVictory)
            return;

        ac.loop = false;
        ac.clip = _clipVictory;
        AudioListener.volume = 0.2f;
        ac.Play();
    }
}
