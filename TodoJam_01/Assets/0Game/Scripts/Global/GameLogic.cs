﻿using System.Collections;
using System.Collections.Generic;
using UltimaCookie.Framework.Core;
using UnityEngine;

public class GameLogic : UCMonoSingleton<GameLogic>
{
    // Start is called before the first frame update
    IEnumerator Start()
    {
        canTick = true;
        
        FishingObjectManager.Get().Initialize();
        PlayerController.Get().Initialize();
        UIManager.Get().Initialize();

        yield return null;
        
        UCSplashScreen.doHide.Invoke();
    }

    // Update is called once per frame
    protected override void OnUpdate()
    {
        // just for quick testing

        if (Input.GetKeyDown(KeyCode.O))
        {
                        
        }
        
        if (Input.GetKeyDown(KeyCode.P))
        {
            
        }
    }
}
