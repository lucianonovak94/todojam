﻿using UnityEngine;

public enum EFishPondType
{
    CELEBRITIES,
    GAMES,
    RANDOM_STUFF,
    WAIFUS
}

public class FishingSpot : MonoBehaviour
{
    public EFishPondType type;
}
