﻿using System;
using System.Collections;
using UltimaCookie.Framework.Events;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class FishingSlide : UIPanel
{
    [NonSerialized]
    public static CustomEventT<bool> onResultDone = new CustomEventT<bool>();
    
    public Slider winMarker;
    public Slider playerSlide;
    public float winOffset = 0.2f;
    public float randomMaxVelocity;

    private float winPoint;

    private float sliderValue;
    private bool fishingResult = false;
    public float randomVelocity = 0.0f;

    public GameObject barContainer;
    public GameObject failMessageContainer;
    public GameObject successMessageContainer;
    
    protected override void BeforeShow()
    {
        PositioningSlide();
        sliderValue = 0.0f;
        randomVelocity = Random.Range(1.0f, randomMaxVelocity);
        
        barContainer.SetActive(true);
        failMessageContainer.SetActive(false);
        successMessageContainer.SetActive(false);

        canTick = true;
    }

    public void PositioningSlide() {
        randomVelocity = Random.Range(1.0f, randomMaxVelocity);
        winPoint = Random.Range(winOffset,1.0f - winOffset);
        winMarker.value = winPoint;
    }

    public bool FishingResult()
    {
        return (playerSlide.value >= (winPoint - winOffset) && 
                playerSlide.value <= (winPoint + winOffset));
    }

    protected override void OnUpdate()
    {
        sliderValue = Mathf.Sin(Time.time * randomVelocity);
        
        if(sliderValue < 0.0f)
        {
            sliderValue *= -1;
        }
        
        playerSlide.value = Mathf.Clamp01(sliderValue);

        if(Input.GetButtonDown(GameConstants.ACTION_BUTTON))
        {
            fishingResult = FishingResult();
            DoResult();
        }
    }

    private void DoResult()
    {
        barContainer.SetActive(false);
        canTick = false;
        
        if (fishingResult)
        {
            successMessageContainer.SetActive(true);
            
            StartCoroutine(WaitRoutine());
            IEnumerator WaitRoutine()
            {
                yield return new WaitForSeconds(1.5f);
                onResultDone.Dispatch(fishingResult);
                Hide();
            }
            
        }
        else
        {
            failMessageContainer.SetActive(true);

            StartCoroutine(WaitRoutine());
            IEnumerator WaitRoutine()
            {
                yield return new WaitForSeconds(1.5f);
                onResultDone.Dispatch(fishingResult);
                Hide();
            }
        }
    }

}
