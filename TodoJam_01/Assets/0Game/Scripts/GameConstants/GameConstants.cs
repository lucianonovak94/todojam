﻿public class GameConstants
{
    public static string UserName = "";
    
    /*BUTTONS*/
    public const string ACTION_BUTTON = "Jump";
    public const string AXIS_X = "Horizontal";
    public const string AXIS_Y = "Vertical";
    public const string EXIT_KEY = "escape";

    public const string PLAYER_NAME_VALUE = "PlayerName";

}
