﻿using UnityEngine;

[CreateAssetMenu(fileName = "FishConfigsScriptable", menuName = "Game/FishConfigsScriptable")]
public class FishConfigsScriptable : ScriptableObject
{
    public Color textColorCommon;
    public Color textColorRare;
    public Color textColorEpic;

    public string textCommon;
    public string textRare;
    public string textEpic;

    public Sprite spriteCommon;
    public Sprite spriteRare;
    public Sprite spriteEpic;

    public string GetTextForRarity(int rarity)
    {
        switch (rarity)
        {
            case 0:
                return textCommon;
            case 1:
                return textRare;
            case 2:
                return textEpic;
        }

        return "";
    }
    
    public Color GetColorForRarity(int rarity)
    {
        switch (rarity)
        {
            case 0:
                return textColorCommon;
            case 1:
                return textColorRare;
            case 2:
                return textColorEpic;
        }

        return textColorCommon;
    }
    
    public Sprite GetSpriteForRarity(int rarity)
    {
        switch (rarity)
        {
            case 0:
                return spriteCommon;
            case 1:
                return spriteRare;
            case 2:
                return spriteEpic;
        }

        return spriteCommon;
    }
}
