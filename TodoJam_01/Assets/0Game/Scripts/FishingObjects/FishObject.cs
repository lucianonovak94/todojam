﻿using UltimaCookie.Framework.Misc;

public class FishObject
{
    private FishObjectConfig _config;
    public FishObjectConfig Config => _config;

    public string id;

    public bool isInOffering;
    
    public FishObject(FishObjectConfig config)
    {
        _config = config;

        id = ToolBox.GetRandomID(15);

        isInOffering = false;
    }
}