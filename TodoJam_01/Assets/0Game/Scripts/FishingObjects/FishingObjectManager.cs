﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using BestHTTP;
using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;
using Sirenix.OdinInspector;
using SRF;
using UC.Managers;
using UltimaCookie.Framework.Core;
using UnityEngine;
using Random = UnityEngine.Random;

public class FishingObjectManager : UCMonoSingleton<FishingObjectManager>
{
    [SerializeField, Required]
    private bool _debugAutoGrantFishes;
    
    
    [SerializeField, Required]
    private TextAsset _jsonObjects;

    [SerializeField]
    private List<FishObjectConfig> _objectConfigs = new List<FishObjectConfig>();

    private List<FishObject> _fishesInventory = new List<FishObject>();
    public List<FishObject> FishesInventory => _fishesInventory;

    [SerializeField]
    private FishConfigsScriptable _fishConfigsScriptable;
    
    public List<FishObject> FishesInOfferInventory()
    {
        List<FishObject> result = new List<FishObject>();
        for (int i = 0; i < _fishesInventory.Count; i++)
        {
            if (_fishesInventory[i].isInOffering)
            {
                result.Add(_fishesInventory[i]);
            }
        }

        return result;
    }

    protected override void CustomInitialize()
    {
        

        StartCoroutine(GetJsonConfig());

        IEnumerator GetJsonConfig()
        {
            
            HTTPRequest request = new HTTPRequest(new Uri("http://kiwiargo.com/wp-content/uploads/2020/07/fish_data.txt"),
                (req, res) =>
                {
                    if (req.State == HTTPRequestStates.Error || req.State == HTTPRequestStates.Aborted ||
                        req.State == HTTPRequestStates.TimedOut
                        || req.State == HTTPRequestStates.ConnectionTimedOut)
                    {
                        InitializeData(_jsonObjects.text);
                    }
                    else
                    {
                        string result = Regex.Unescape(@res.DataAsText);
                        InitializeData(result);    
                    }
                });
            request.MethodType = HTTPMethods.Get;
            request.SetHeader("Content-Type", "application/json");
            request.Send();
            
            yield break;
            
            // InitializeData(_jsonObjects.text);
        }
    }

    private void InitializeData(string jsonText)
    {
        _objectConfigs = JsonConvert.DeserializeObject<List<FishObjectConfig>>(jsonText);
        
        _objectConfigs.ForEach(fishConfig => { fishConfig.Init(); });

        if (_debugAutoGrantFishes)
        {
            foreach (var config in _objectConfigs)
            {
                FishObject newFish = new FishObject(config);
                
                config.Init();
                config.LoadData(() => { });
                
                _fishesInventory.Add(newFish);
            }
        }
    }

    public void SendOffer()
    {
        //send fish to server
        _fishesInventory.Clear();
    }

    public void GrantRandomFishForType(EFishPondType pondType)
    {
        int randomRarity = Random.Range(0, 100);

        List<FishObjectConfig> listToUse = null;
        
        // Legendary 5%
        if (randomRarity < 5)
        {
            listToUse = _objectConfigs.Where(oc => oc.rarity == 2 && oc.type == pondType).ToList();
        }
        // Rare 15%
        else if (randomRarity < 20)
        {
            listToUse = _objectConfigs.Where(oc => oc.rarity == 1 && oc.type == pondType).ToList();
        }
        // Common 80%
        else
        {
            listToUse = _objectConfigs.Where(oc => oc.rarity == 0 && oc.type == pondType).ToList();
        }

        FishObjectConfig fishToGrantConfig = listToUse.Random();
        
        FishObject addedFish = new FishObject(fishToGrantConfig);
        _fishesInventory.Add(addedFish);
        
        fishToGrantConfig.LoadData(() =>
        {
            UIManager.Get().ShowFishAcquired(fishToGrantConfig);
        });
    }

    public void ConfirmOffering()
    {
        var offeringFishes = FishesInOfferInventory();
        
        string offeringDataString = "";

        foreach (var fishOffer in offeringFishes)
        {
            offeringDataString += $"{fishOffer.Config.name} ({_fishConfigsScriptable.GetTextForRarity(fishOffer.Config.rarity)}) <> ";
        }
        
        ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();
        request.FunctionName = "ConfirmOffering";
        request.FunctionParameter = new {offeringData = offeringDataString };

        PlayFabClientAPI.ExecuteCloudScript(request, result =>
        {
            MusicManager.Get().PlayMasterChatMusic();
            
            // TODO: hacer transicion y mostrar chat
            UIChatCanvas.Get().OpenChat();
            
        }, error => { ConfirmOffering(); });
        
    }

}
