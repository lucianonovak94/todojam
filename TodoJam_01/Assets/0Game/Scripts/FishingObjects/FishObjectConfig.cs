﻿using System;
using System.Collections;
using UltimaCookie.Framework.Core;
using UnityEngine;
using UnityEngine.Networking;

[Serializable]
public class FishObjectConfig
{
    public string id;
    public string name;
    public int rarity;
    public string description;
    public string image;

    public Texture texture = null;
    public Sprite sprite = null;

    public EFishPondType type;
    public string typeString;

    public void Init()
    {
        Enum.TryParse(typeString, out type);
    }
    
    public void LoadData(Action callback)
    {
        if (texture != null)
        {
            callback.Invoke();
            return;
        }

        CoroutinesRunner.RunCoroutine(GetTexture());
        
        IEnumerator GetTexture() {
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(image);
            yield return www.SendWebRequest();

            if(www.isNetworkError || www.isHttpError) {
                Debug.LogError(www.error);
            }
            else {
                texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                sprite = Sprite.Create(texture as Texture2D, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f), 1 );
                callback.Invoke();
            }
        }
    }

    

    
}
