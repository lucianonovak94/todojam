﻿public class APlayerState
{
    protected PlayerController _player;

    public APlayerState()
    {
        _player = PlayerController.Get();
    }

    public virtual void StartExecution()
    {
    }

    public virtual void Update()
    {
    }

    public virtual void FinishExecution()
    {
    }
}