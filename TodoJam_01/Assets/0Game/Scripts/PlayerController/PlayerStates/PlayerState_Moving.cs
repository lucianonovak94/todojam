﻿using UnityEngine;

public class PlayerState_Moving : APlayerState
{
    private Vector3 movVector;
    
    public override void StartExecution()
    {
        movVector = new Vector3();
        _player.animator.SetTrigger("StartMoving");
        UIManager.Get().ShowMovingHUD();
    }

    public override void Update()
    {
        if(_player.currentState == this)
        {
            Movement();
            CheckFishing();

            if (Input.GetKeyDown(KeyCode.Tab))
            {
                UIManager.Get().ShowInventory();
                _player.ChangeState<PlayerState_InUIMenu>();
            }
        }
    }

    public override void FinishExecution()
    {
    }
    
    private void Movement()
    {
        movVector.x = Input.GetAxisRaw(GameConstants.AXIS_X);
        movVector.z = Input.GetAxisRaw(GameConstants.AXIS_Y);
        _player.animator.SetFloat("VerticalMovement", movVector.z);
        _player.animator.SetFloat("HorizontalMovement", movVector.x);
        _player.movementDirection = movVector.normalized * _player.walkSpeed * Time.deltaTime;;
        _player.characterController.SimpleMove(_player.movementDirection);
    }

    private void CheckFishing()
    {
        if(_player.nearPond && Input.GetButtonDown(GameConstants.ACTION_BUTTON))
        {
            _player.ChangeState<PlayerState_FishingCatch>();
        } 
        else if(_player.nearShrine && Input.GetButtonDown(GameConstants.ACTION_BUTTON)) 
        {
            _player.ChangeState<PlayerState_Shrine>();
        }
    }
}