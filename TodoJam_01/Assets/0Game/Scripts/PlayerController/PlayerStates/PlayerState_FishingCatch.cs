﻿using UC.Managers;
using UnityEngine;

public class PlayerState_FishingCatch : APlayerState
{
    public override void StartExecution()
    {
        _player.interactionDirection = CheckInteractionDirection();

        if (_player.interactionDirection == Vector3.forward)
        {
            _player.animator.SetTrigger("StartFishing_Top");
        }
        else if (_player.interactionDirection == Vector3.right)
        {
            _player.animator.SetTrigger("StartFishing_Right");
        }
        else if (_player.interactionDirection == Vector3.back)
        {
            _player.animator.SetTrigger("StartFishing_Bottom");
        } 
        else if (_player.interactionDirection == Vector3.left)
        {
            _player.animator.SetTrigger("StartFishing_Left");
        }

        UIManager.Get().ShowCatch();
        UICatchFish.onCatchDone.AddListener(OnCatchResult);
    }

    public override void Update()
    {

    }

    public override void FinishExecution()
    {
        UICatchFish.onCatchDone.RemoveListener(OnCatchResult);
    }

    private void OnCatchResult(bool result)
    {
        if(result)
        {
            UCAnalytics.PlayfabLogEvent("FISH_CATCH_STARTED");
            _player.ChangeState<PlayerState_Fishing>();
        }
        else
        {
            UCAnalytics.PlayfabLogEvent("TIME_OUT_CATCH");
            //fail state
            _player.ChangeState<PlayerState_Moving>();
        }
    }

        private Vector3 CheckInteractionDirection()
    {
        Transform sensorTransform = _player.sensor.transform;
        LayerMask sensorMask = _player.sensor.maskInteractables;
        
        Ray ray = new Ray();
        ray.origin = sensorTransform.position;
        ray.direction = sensorTransform.right;
        RaycastHit hitInfo;
        float checkDistance = 1.6f;
        
        if (Physics.Raycast(ray, out  hitInfo, checkDistance, sensorMask.value))
        {
            return Vector3.right;
        }
        
        ray.direction = -sensorTransform.forward;
        
        if (Physics.Raycast(ray, out hitInfo, checkDistance, sensorMask.value))
        {
            return Vector3.back;
        }
        
        ray.direction = -sensorTransform.right;
        
        if (Physics.Raycast(ray, out hitInfo, checkDistance, sensorMask.value))
        {
            return Vector3.left;
        }
        
        return Vector3.forward;
    }
}
