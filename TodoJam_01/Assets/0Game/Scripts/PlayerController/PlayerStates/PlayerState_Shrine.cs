﻿public class PlayerState_Shrine : APlayerState
{
    public override void StartExecution()
    {
        UIManager.Get().ShowShrine();
        UIShrine.onOferingDone.AddListener(OnOferingDone);
    }

    public override void Update()
    {
    }

    public override void FinishExecution()
    {
    }

    public void OnOferingDone(bool result)
    {
        UIShrine.onOferingDone.RemoveListener(OnOferingDone);

        if(result)
        {
            //TODO win state
            FishingObjectManager.Get().SendOffer();
            UIManager.Get().ShowInventory();
        }
        else
        {
            //TODO lose state
            _player.ChangeState<PlayerState_Moving>();
        }
    }
}
