﻿using System.Collections;
using UC.Managers;
using UltimaCookie.Framework.Core;
using UnityEngine;

public class PlayerState_Fishing : APlayerState
{
    public override void StartExecution()
    {
        UCAnalytics.PlayfabLogEvent("START_FISHING");
        
        FishingTransitionBlit.onTransitionFinished.RemoveAllListeners();
        FishingTransitionBlit.onTransitionFinished.AddListener(() =>
        {
            MusicManager.Get().PlayFightMusic();
            UIManager.Get().ShowFishingUI();
            FishingSlide.onResultDone.AddListener(OnFishingDone);    
        });
        
        FishingTransitionBlit.Get().StartTransition();
    }


    
    public override void Update()
    {
    }

    public override void FinishExecution()
    {
    }

    private void OnFishingDone(bool result)
    {
        FishingSlide.onResultDone.RemoveListener(OnFishingDone);

        
        
        if (result)
        {
            UCAnalytics.PlayfabLogEvent("FISH_SLIDER_WIN");

            if (_player.interactingSpot == null)
            {
                FishingObjectManager.Get().GrantRandomFishForType(EFishPondType.RANDOM_STUFF);    
            }
            else
            {
                FishingObjectManager.Get().GrantRandomFishForType(_player.interactingSpot.type);    
            }

            MusicManager.Get().PlayVictoryMusic();
            
            CoroutinesRunner.RunCoroutine(StopMusic());
            
            IEnumerator StopMusic()
            {
                yield return new WaitForSeconds(6f);
                MusicManager.Get().PlayBGMusic();
            }

        }
        else
        {
            MusicManager.Get().PlayBGMusic();
            
            UCAnalytics.PlayfabLogEvent("FISH_SLIDER_LOSE");
            // TODO: do fail effect
            _player.ChangeState<PlayerState_Moving>();
        }
    }
}