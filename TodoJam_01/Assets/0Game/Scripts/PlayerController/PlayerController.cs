﻿using System;
using UltimaCookie.Framework.Core;
using UltimaCookie.Framework.Extensions;
using UnityEngine;

public class PlayerController : UCMonoSingleton<PlayerController>
{
    public bool nearPond;
    public Vector3 interactionDirection;
    public FishingSpot interactingSpot;
    
    public bool nearShrine;
    public float walkSpeed = 1000f;
    public CharacterController characterController;

    public Vector3 movementDirection;
    
    public APlayerState currentState;

    public InteractionSensor sensor;
    
    public Animator animator;

    protected override void CustomInitialize()
    {
        Camera.main.transparencySortMode = TransparencySortMode.Orthographic;
        
        characterController = gameObject.GetComponentChecked<CharacterController>();
        animator = gameObject.GetComponentChecked<Animator>();
        sensor = gameObject.GetComponentCheckedInChildren<InteractionSensor>();
        
        currentState = new PlayerState_Moving();  
        currentState.StartExecution();
    }

    public void ChangeState<T>() where T : APlayerState
    {
        if(currentState is T)
            return;
        
        currentState.FinishExecution();

        currentState = Activator.CreateInstance<T>();
        currentState.StartExecution();
    }

    protected override void OnUpdate()
    {
        currentState.Update();
    }
}
