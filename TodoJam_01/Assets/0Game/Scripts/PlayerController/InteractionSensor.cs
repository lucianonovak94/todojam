﻿using UnityEngine;

public class InteractionSensor : MonoBehaviour
{
    private PlayerController _playerController;

    public LayerMask maskInteractables;
    
    private void Start()
    {
        _playerController = GetComponentInParent<PlayerController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        var fishingPond = other.GetComponent<FishingSpot>();
        
        if (fishingPond != null)
        {
            _playerController.nearPond = true;
            _playerController.interactingSpot = fishingPond;
            
            UIMovingHUD.onSetTooltipText.Dispatch("ESPACIO PARA PESCAR");
        }
        else if (other.GetComponent<Shrine>() != null)
        {
            _playerController.nearShrine = true;
            UIMovingHUD.onSetTooltipText.Dispatch("ESPACIO PARA OFRENDAR");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<FishingSpot>() != null)
        {
            _playerController.interactingSpot = null;
            _playerController.nearPond = false;    
        }
        else if (other.GetComponent<Shrine>() != null)
        {
            _playerController.nearShrine = false;
        }
        
        UIMovingHUD.onTooltipHidden.Dispatch();
    }
}
