﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UltimaCookie.Framework.Core;
using UltimaCookie.Framework.Extensions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class ChatPanel : UCMonoSingleton<ChatPanel>
{
    [SerializeField, Required]
    private Transform _tChatRowsContainer;
    
    // I'm using this one to place the new chat rows here temporarily (it's far away the screen)
    // so they can create and update properly (it's just a frame), and then it's moved to the
    // actual container
    [SerializeField, Required]
    private Transform _tTempVisualContainerForRows;

    [SerializeField, Required]
    private TMP_InputField _fieldInputMessage;
    
    [SerializeField, Required]
    private ChatMessageRow _prefabMessageRow;

    private ScrollRect _scrollView;
    
    protected override void CustomInitialize()
    {
        base.CustomInitialize();

        instance = this;
        
        canTick = true;
        
        _fieldInputMessage.onSubmit.AddListener(OnSubmitMessage);

        _scrollView = GetComponentInChildren<ScrollRect>();
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void ClearChat()
    {
        foreach (Transform t in _tChatRowsContainer)
        {
            Destroy(t.gameObject);
        }
        
        Debug.Log("Clearing Chat");
    }

    public void AddMessage(string username, string msg)
    {
        if(!enabled || !gameObject.activeSelf || !gameObject.activeInHierarchy)
            return;

        StartCoroutine(SetupRow());
        
        IEnumerator SetupRow()
        {
            ChatMessageRow newRow = Instantiate(_prefabMessageRow, _tTempVisualContainerForRows);
            
            newRow.Initialize(username, msg);
            yield return null;
            newRow.gameObject.SetActive(true);
            yield return null;
            newRow.Refresh();
            
            yield return null;

            newRow.transform.SetParent(_tChatRowsContainer);
            newRow.transform.SetAsLastSibling();

            yield return null;
            
            newRow.Refresh();
            
            _scrollView.normalizedPosition = new Vector2(0, 0);
        }
    }

    public void OnClick_SendMessage()
    {
        OnSubmitMessage(_fieldInputMessage.text);
    }

    private void OnSubmitMessage(string msg)
    {
        PlayerChatManager.Get().SendChatMessage(msg);
        
        _fieldInputMessage.text = "";
    }

    protected override void OnUpdate()
    {
        if(_fieldInputMessage.isFocused && _fieldInputMessage.text != "" && Input.GetKeyDown(KeyCode.Return)) 
        {
            OnSubmitMessage(_fieldInputMessage.text);
        }
    }
}
