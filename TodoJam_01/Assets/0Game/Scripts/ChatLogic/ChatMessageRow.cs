﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

public class ChatMessageRow : MonoBehaviour
{

    [SerializeField, Required]
    private TextMeshProUGUI _textUserName;
    
    [SerializeField, Required]
    private TextMeshProUGUI _textMessage;

    public void Initialize(string userName, string msg)
    {
        _textMessage.autoSizeTextContainer = true;
        
        _textUserName.text = userName;
        _textMessage.text = msg;
    }

    public void Refresh()
    {
        float height = _textMessage.textBounds.extents.y * 2;
        _textMessage.rectTransform.sizeDelta = new Vector2(_textMessage.rectTransform.sizeDelta.x, height);
    }
}
