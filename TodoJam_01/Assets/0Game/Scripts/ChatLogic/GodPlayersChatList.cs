﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PlayFab;
using PlayFab.ClientModels;
using Sirenix.OdinInspector;
using TMPro;
using UltimaCookie.Framework.Core;
using UnityEngine;
using UnityEngine.UI;

public class GodPlayersChatList : UCMonoSingleton<GodPlayersChatList>
{
    [SerializeField, Required]
    private Transform _tContainerList;

    [SerializeField, Required]
    private Button _addUserBtn;

    [SerializeField, Required]
    private Button _btnRefresh;
    
    [SerializeField, Required]
    private TMP_InputField _inputFieldUserName;
    
    [SerializeField, Required]
    private UIBtnPlayerToChatWith _prefabPlayerBtn;

    private string _usersList;

    public TextMeshProUGUI textOffering;
    
    private List<UIBtnPlayerToChatWith> _btnsList = new List<UIBtnPlayerToChatWith>();
    
    // Start is called before the first frame update
    void Start()
    {
        _addUserBtn.onClick.AddListener(OnClick_AddUser);
        _btnRefresh.onClick.AddListener(GetUsersList);
        
        GetUsersList();

        StartCoroutine(RefreshChatRoutine());
    }

    private IEnumerator RefreshChatRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(60f);
            // GetUsersList();
        }
    }

    private void GetUsersList()
    {
        foreach (Transform btnT in _tContainerList)
        {
            Destroy(btnT.gameObject);
        }
        
        // GetTitleDataRequest request = new GetTitleDataRequest();
        // request.Keys = new List<string>();
        // request.Keys.Add("chatUsers");
        //
        // PlayFabClientAPI.GetTitleData(request, result =>
        // {
        //     _usersList = result.Data["chatUsers"];
        //     RefreshVisualList();
        // }, error =>
        // {
        //     Debug.LogError("[GodPlayersChatList] Error retrieving list => " + error.ErrorMessage);
        // });
        
        ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();
        request.FunctionName = "GetUsersList";
        
        PlayFabClientAPI.ExecuteCloudScript(request, result =>
        {
            _usersList = result.FunctionResult.ToString();
             RefreshVisualList();
            
        }, error =>
        {
            Debug.LogError("[GodPlayersChatList] Error retrieving list => " + error.ErrorMessage);
        });
    }

    private void RefreshVisualList()
    {
        PlayerChatManager.onReceivedMessages.RemoveAllListeners();
        
        foreach (Transform btnT in _tContainerList)
        {
            Destroy(btnT.gameObject);
        }

        string[] userNames = _usersList.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries);

        userNames = userNames.Reverse().ToArray();
        
        foreach (var userName in userNames)
        {
            PlayerChatManager.Get().ConnectToRoomForPlayer(userName);
            
            UIBtnPlayerToChatWith newBtn = Instantiate(_prefabPlayerBtn, _tContainerList);
            newBtn.Initialize(userName);
        }
        
    }

    private void OnClick_AddUser()
    {
        string newUserName = _inputFieldUserName.text.Trim();
        _inputFieldUserName.text = "";

        if(_usersList.Contains(newUserName))
            return;
        
        _usersList += "," + newUserName;
        
        ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();
        request.FunctionName = "SetUsersList";
        request.FunctionParameter = new {list = _usersList}; 
        
        PlayFabClientAPI.ExecuteCloudScript(request, 
            result =>
            {
                GetUsersList();
            }, error =>
            {
                Debug.LogError(error.ErrorMessage);
            });
    }
}
