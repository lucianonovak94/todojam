﻿using UltimaCookie.Framework.Core;
using UnityEngine;

[CreateAssetMenu(fileName = "ChatConfig", menuName = "Game/ChatConfig")]
public class ChatConfig : ScriptableObject
{
    public bool testClient = false;
    public bool isClient = true;
}
