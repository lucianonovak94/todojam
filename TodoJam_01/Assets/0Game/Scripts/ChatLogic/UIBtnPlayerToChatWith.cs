﻿using System;
using LitJson;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIBtnPlayerToChatWith : MonoBehaviour
{
    [SerializeField, Required]
    private TextMeshProUGUI _textPlayerID;

    [SerializeField, Required]
    private TextMeshProUGUI _textPlayerName;

    [SerializeField, Required]
    private GameObject _notificationObject;
    
    private string _userID;
    private string _userName;
    private string _offeringData;

    public void Initialize(string userID)
    {
        _notificationObject.SetActive(false);
        
        _userID = userID;

        _textPlayerID.text = userID;
        
        GetComponent<Button>().onClick.AddListener(() =>
        {
            ChatPanel.Get().ClearChat();
            GodPlayersChatList.Get().textOffering.text = _offeringData;
            _notificationObject.SetActive(false);
            PlayerChatManager.Get().OpenChatForPlayer(_userID);
        });
        
        PlayerChatManager.onReceivedMessages.AddListener((channelName) =>
        {
            if (channelName == "chat_" + _userID)
            {
                _notificationObject.SetActive(true);
            }

        });
        
        
        ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();
        request.FunctionName = "GetUserFinalData";
        request.FunctionParameter = new {targetID = _userID};
        
        PlayFabClientAPI.ExecuteCloudScript(request, result =>
        {
            try
            {
                JsonObject jsonResult = (JsonObject)result.FunctionResult;
                object userName;
                jsonResult.TryGetValue("username", out userName);

                if (userName != null)
                {
                    _userName = userName.ToString();
                    _textPlayerName.text = _userName;
                }

                object offering;
                jsonResult.TryGetValue("offeringData", out offering);

                if (offering != null)
                {
                    _offeringData = offering.ToString();
                }
            }
            catch (Exception e)
            {
                // Console.WriteLine(e);
                // throw;
            }

        }, error =>
        {
            Debug.LogError(error.ErrorMessage);
        });
    }
}
