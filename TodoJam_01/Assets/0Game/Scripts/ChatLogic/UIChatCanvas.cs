﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UltimaCookie.Framework.Core;
using UnityEngine;

public class UIChatCanvas : UCMonoSingleton<UIChatCanvas>
{   
    [SerializeField, Required]
    private GameObject _chatPanel;

    [SerializeField, Required]
    private GameObject _overlay;
    
    void Start()
    {
        _overlay.SetActive(false);
        _chatPanel.SetActive(false);
    }

    public void OpenChat()
    {
        _overlay.SetActive(true);
        _chatPanel.SetActive(true);
    }
}
