﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExitGames.Client.Photon;
using Photon.Chat;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.GroupsModels;
using UltimaCookie.Framework.Core;
using UltimaCookie.Framework.Events;
using UltimaCookie.Framework.Extensions;
using UnityEngine;

public class PlayerChatManager : UCMonoSingleton<PlayerChatManager>, IChatClientListener
{
    public static CustomEventT<string> onReceivedMessages = new CustomEventT<string>(); 
    
    private ChatClient _chatClient;
    
    [SerializeField]
    private ChatSettings _chatAppSettings;

    private string _otherUserID;
    private string _myUserName;
    private string _channelName;

    private ChatConfig chatConfig;

    public ChatPanel chatPanel;
    
    private Dictionary<string, List<string>> _messagesByChat = new Dictionary<string, List<string>>();
    
    private void Start()
    {
        Initialize();
    }

    protected override void CustomInitialize()
    {
        chatPanel.Initialize();
        
        _messagesByChat.Clear();
        
        chatConfig = Resources.Load<ChatConfig>("ChatConfig");

        canTick = false;
        
        if (!chatConfig.isClient)
        {
            ConnectToRoom();
        }
    }

    public void ConnectToRoom()
    {
        string username = GameConstants.UserName;
        _myUserName = chatConfig.isClient ? username : "DIOS";
        _otherUserID = chatConfig.isClient ? "DIOS" : username; 

        _channelName = "";
        
        Debug.Log("[PlayerChatManager] Connecting To Chat: " + BackendManager.playfabID);
        _chatClient = new ChatClient(this);
        _chatClient.ChatRegion = "US";
        
        _chatClient.Connect(_chatAppSettings.AppId, "1.0", new AuthenticationValues(_myUserName));

        canTick = true;
    }


    // ONLY SERVER
    public void ConnectToRoomForPlayer(string playerID)
    {
        _chatClient.Subscribe("chat_" + playerID, 0, 10);
    }

    public void OpenChatForPlayer(string playerID)
    {
        ChatPanel.Get().ClearChat();
        _channelName = "chat_" + playerID;
        _chatClient.Subscribe("chat_" + playerID, 0, 10);

        if (_messagesByChat.ContainsKey(_channelName))
        {
            foreach (var msg in _messagesByChat[_channelName])
            {
                string[] parts = msg.Split(new string[]{"[===]"}, StringSplitOptions.RemoveEmptyEntries);
            
                if(parts.Length != 2)
                    continue;
                
                ChatPanel.Get().AddMessage(parts[0], parts[1]);    
            }
        }
    }

    public void SendChatMessage(string msg)
    {
        _chatClient.PublishMessage(_channelName, msg);
        
        UpdateUserDataRequest request = new UpdateUserDataRequest();
        request.Data = new Dictionary<string, string>
        {
            {"messages" , msg}
        };
        
        PlayFabClientAPI.UpdateUserData(request, result => { }, error => { });
        
    }

    protected override void OnUpdate()
    {
       _chatClient.Service();
    }
    
    public void DebugReturn(DebugLevel level, string message)
    {
        
    }

    public void OnDisconnected()
    {
        Debug.Log("[PlayerChatManager] Player DISCONNECTED");
    }

    public void OnConnected()
    {
        Debug.Log("[PlayerChatManager] Player CONNECTED");
        
        Debug.Log("[PlayerChatManager] Joining to channel: " + _channelName);

        if (chatConfig.isClient)
        {
            _channelName = "chat_" + BackendManager.playfabID;
            _chatClient.Subscribe(_channelName);
        }
    }

    public void OnChatStateChange(ChatState state)
    {
        Debug.Log("[PlayerChatManager] chat stage change = " + state.ToString());
    }

    public void OnGetMessages(string channelName, string[] senders, object[] messages)
    {
        for (int i = 0; i < messages.Length; i++)
        {
            string chatName = channelName;

            if (!_messagesByChat.ContainsKey(chatName))
            {
                _messagesByChat.Add(chatName, new List<string>());
            }

            _messagesByChat[chatName].Add(senders[i] + "[===]" + messages[i].ToString());
        }
        
        if (channelName != _channelName)
        {
            onReceivedMessages.Dispatch(channelName);
            return;
        }
        
        foreach (var msg in messages)
        {
            ChatPanel.Get().AddMessage(senders[0], msg.ToString());    
        }
    }

    public void OnPrivateMessage(string sender, object message, string channelName)
    {
        Debug.Log("[PlayerChatManager] Received Message = " + message.ToString());
        ChatPanel.Get().AddMessage(sender, message.ToString());
    }

    public void OnSubscribed(string[] channels, bool[] results)
    {
        Debug.Log("[PlayerChatManager] Joined to channel: " + _channelName);
        // Debug.LogError(_chatClient.PublicChannels[_channelName].Messages.Count);

    }

    public void OnUnsubscribed(string[] channels)
    {
        
    }

    public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
    {
        Debug.Log("[PlayerChatManager] Status Updated: " + _channelName);
    }

    public void OnUserSubscribed(string channel, string user)
    {
        Debug.Log("[PlayerChatManager] Joined to channel: " + _channelName);
    }

    public void OnUserUnsubscribed(string channel, string user)
    {
        
    }
}
