﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UltimaCookie.Framework.Events;
using UnityEngine;

public class UIMovingHUD : UIPanel
{
    public static readonly CustomEventT<string> onSetTooltipText = new CustomEventT<string>();
    public static readonly CustomEvent onTooltipHidden = new CustomEvent();
    
    [SerializeField, Required]
    private TextMeshProUGUI _textTooltip;

    public override void Initialize()
    {
        onTooltipHidden.AddListener(() => { _textTooltip.text = ""; });
        onSetTooltipText.AddListener((tooltip) => { _textTooltip.text = tooltip;});
        
        onTooltipHidden.Dispatch();
    }

    protected override void BeforeShow()
    {
        onTooltipHidden.Dispatch();
    }
}
