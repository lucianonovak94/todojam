﻿using System;
using System.Collections;
using System.Collections.Generic;
using UltimaCookie.Framework.Events;
using UltimaCookie.Framework.Core;
using UnityEngine;
using Random = UnityEngine.Random;
using TMPro;

public class UICatchFish : UIPanel
{
    private float dotInterval = 1.0f;
    public float timeToCatch = 2.0f;
    public float holderTime;
    public bool catchResult;
    public bool inCatchTime = false;
    public TextMeshProUGUI gatchaText;
    public Animator animator;
    
    private float actualTime = 0.0f;

    [NonSerialized]
    public static CustomEventT<bool> onCatchDone = new CustomEventT<bool>();

    protected override void AfterShow()
    {
        animator.gameObject.SetActive(false);

        StartCoroutine(HoldingRoutine());
    }

    public override void Initialize()
    {
        Hide();
    }

    IEnumerator HoldingRoutine()
    {
        gatchaText.text = "";
        holderTime = Random.Range(1.0f, 3.0f);
        float currentHolderTime = 0.0f;
        float dotTime = 0.0f;
        while (currentHolderTime < holderTime)
        {
            dotTime += Time.deltaTime;
            if(dotTime >= dotInterval)
            {
                gatchaText.text += " .";
                dotTime = 0.0f;
            }
            currentHolderTime += Time.deltaTime;
            yield return null;    
        }
        float currentCatchTime = 0.0f;
        gatchaText.text = "Apreta Espacio!!!";
        animator.gameObject.SetActive(true);

        while (currentCatchTime <= timeToCatch)
        {
            currentCatchTime += Time.deltaTime;
            if(Input.GetButtonDown(GameConstants.ACTION_BUTTON))
            {
                gatchaText.text = "Picó!!!";
                yield return new WaitForSeconds(0.5f);
                onCatchDone.Dispatch(true);
                Hide();
                yield break;
            }
            yield return null;
        }
        animator.gameObject.SetActive(false);
        gatchaText.text = "Se escapo :C";
        yield return new WaitForSeconds(0.5f);
        onCatchDone.Dispatch(false);
        Hide();
    }
}
