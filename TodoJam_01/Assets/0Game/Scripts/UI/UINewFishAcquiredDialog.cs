﻿using System.Collections;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using Button = UnityEngine.UI.Button;
using Cursor = UnityEngine.Cursor;
using Image = UnityEngine.UI.Image;

public class UINewFishAcquiredDialog : UIPanel
{
    [SerializeField, Required]
    private TextMeshProUGUI _textFishName;
    
    [SerializeField, Required]
    private TextMeshProUGUI _textFishDescription;
    
    [SerializeField, Required]
    private TextMeshProUGUI _textRarity;
    
    [SerializeField, Required]
    private Image _imgFishPicture;
    
    [SerializeField, Required]
    private Image _imgFishBackground;
    
    [SerializeField, Required]
    private Button _btnAccept;

    private bool canExit = false;

    private FishConfigsScriptable _fishConfigs;
    
    public override void Initialize()
    {
        canExit = false;
        _btnAccept.onClick.AddListener(OnClick_Accept);
        _fishConfigs = Resources.Load<FishConfigsScriptable>("FishConfigsScriptable");

        _imgFishBackground = _imgFishPicture.transform.parent.GetComponent<Image>();
        
        Hide();
    }

    protected override void OnUpdate()
    {
        if(!canExit)
        {
            return;
        }

        if(Input.GetButtonDown(GameConstants.ACTION_BUTTON))
        {
            Cursor.visible = false;
            PlayerController.Get().ChangeState<PlayerState_Moving>();
            Hide();
        }
    }

    public void Show(FishObjectConfig newFish)
    {
        _textFishName.text = newFish.name;
        _textFishDescription.text = newFish.description;

        _textRarity.text = _fishConfigs.GetTextForRarity(newFish.rarity);
        _textRarity.color = _fishConfigs.GetColorForRarity(newFish.rarity);
            
        Cursor.visible = true;

        _imgFishPicture.sprite = newFish.sprite;

        _imgFishBackground.sprite = _fishConfigs.GetSpriteForRarity(newFish.rarity);
        
        Show();
        
        StartCoroutine(CanExitTrigger());
    }

    IEnumerator CanExitTrigger()
    {
        yield return new WaitForSeconds(2.0f);
        canExit = true;
    }

    private void OnClick_Accept()
    {
        Cursor.visible = false;
        PlayerController.Get().ChangeState<PlayerState_Moving>();
        Hide();
    }
}
