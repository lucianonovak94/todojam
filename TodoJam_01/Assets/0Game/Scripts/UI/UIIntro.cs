﻿using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using Sirenix.OdinInspector;
using TMPro;
using UC.Managers;
using UltimaCookie.Framework.Events;
using UnityEngine;
using UnityEngine.UI;

public class UIIntro : UIPanel
{

    [SerializeField, Required]
    private TextMeshProUGUI _playerName;

    [SerializeField, Required]
    private Button _btnBegin;

    [SerializeField, Required]
    private GameObject _goErrorText;
    
    public override void Initialize()
    {
        Hide();
        PlayerController.Get().ChangeState<PlayerState_InUIMenu>();
        _btnBegin.onClick.AddListener(OnBegins);
        _goErrorText.SetActive(false);
    }

    protected override void BeforeShow()
    {

    }

    private void OnBegins()
    {
        if (_playerName.text.Length < 4)
        {
            _goErrorText.SetActive(true);
            return;
        }

        _goErrorText.SetActive(false);
        _btnBegin.gameObject.SetActive(false);
        
        PlayerPrefs.SetString(GameConstants.PLAYER_NAME_VALUE, _playerName.text);
        GameConstants.UserName = _playerName.text;
        
        UIManager.Get().ShowMovingHUD();
        PlayerController.Get().ChangeState<PlayerState_Moving>();
        
        UCAnalytics.PlayfabLogEvent("INTRO_DONE");
        
        PlayerChatManager.Get().ConnectToRoom();
        
        SetUserNameInServer(_playerName.text);   
    }

    private void SetUserNameInServer(string username)
    {
        UpdateUserDataRequest request = new UpdateUserDataRequest();
        request.Data = new Dictionary<string, string> {{"username", username}};
        
        Hide();
        
        PlayFabClientAPI.UpdateUserData(request,
            result =>
            {
                ExecuteCloudScriptRequest req = new ExecuteCloudScriptRequest();
                req.FunctionName = "NotifyName";
                
                PlayFabClientAPI.ExecuteCloudScript(req, scriptResult =>  { }, error => { });

            }, 
            error =>
            {
                SetUserNameInServer(username);               
            });
    }

}
