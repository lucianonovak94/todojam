﻿using System;
using Sirenix.OdinInspector;
using UltimaCookie.Framework.Events;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIInventorySlot : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField, Required]
    private Image _icon;
    
    private FishObject _fish;
    public FishObject Fish => _fish;

    [NonSerialized]
    public readonly CustomEventT<UIInventorySlot> onClicked = new CustomEventT<UIInventorySlot>();
    
    [NonSerialized]
    public readonly CustomEventT<UIInventorySlot> onHoverEnter = new CustomEventT<UIInventorySlot>();
    
    [NonSerialized]
    public readonly CustomEventT<UIInventorySlot> onHoverExit = new CustomEventT<UIInventorySlot>();
    
    public void Initialize(FishObject fish)
    {
        _fish = fish;

        _icon.sprite = fish.Config.sprite;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        onClicked.Dispatch(this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        onHoverEnter.Dispatch(this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        onHoverExit.Dispatch(this);
    }
}
