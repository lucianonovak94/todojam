﻿using Sirenix.OdinInspector;
using UC.Managers;
using UltimaCookie.Framework.Extensions;
using UnityEngine;
using UnityEngine.UI;

public class UIInventory : UIPanel
{
    public static UIInventory instance;
    
    [SerializeField, Required]
    private Transform _tParentInventory;

    [SerializeField, Required]
    private UIInventorySlot _prefabInventorySlot;
    
    [SerializeField, Required]
    private UIInventoryDetailsPanel _panelFishDetails;

    [SerializeField, Required]
    private Button _btnClose;

    public override void Initialize()
    {
        instance = this;
        Hide();
        _btnClose.onClick.AddListener(OnClick_Close);
        _panelFishDetails.Initialize();
    }

    protected override void BeforeShow()
    {
        UCAnalytics.PlayfabLogEvent("INVENTORY_OPEN");
        
        foreach (Transform t in _tParentInventory)
        {
            Destroy(t.gameObject);
        }

        var inventory = FishingObjectManager.Get().FishesInventory;

        foreach (var fish in inventory)
        {
            Transform targetContainer = fish.isInOffering ? UIShrine.instance.TParentOffering : _tParentInventory;

            UIInventorySlot newSlot = Instantiate(_prefabInventorySlot, targetContainer);
            newSlot.Initialize(fish);
            
            newSlot.onClicked.AddListener(OnSlotClicked);
            newSlot.onHoverEnter.AddListener(OnSlotHoverEnter);
            newSlot.onHoverExit.AddListener(OnSlotHoverExit);
        }

        Cursor.visible = true;
    }

    private void OnSlotClicked(UIInventorySlot clickedSlot)
    {
        if (!UIShrine.instance.gameObject.active)
        {
            return;
        }
            
        
        if (clickedSlot.Fish.isInOffering)
        {
            clickedSlot.Fish.isInOffering = false;
            
            clickedSlot.SetParent(_tParentInventory);
        }
        else
        {
            if (UIShrine.instance.TParentOffering.childCount < 5)
            {
                clickedSlot.Fish.isInOffering = true;
            
                clickedSlot.SetParent(UIShrine.instance.TParentOffering);    
            }
        }
    }

    private void OnSlotHoverEnter(UIInventorySlot clickedSlot)
    {
        if(UIShrine.instance.gameObject.active)
            return;
        
        _panelFishDetails.Show(clickedSlot.Fish);
    }
    
    private void OnSlotHoverExit(UIInventorySlot clickedSlot)
    {
        if(UIShrine.instance.gameObject.active)
            return;
        
        _panelFishDetails.HideFrom(clickedSlot.Fish);
    }

    private void OnClick_Close()
    {
        UCAnalytics.PlayfabLogEvent("INVENTORY_CLOSE");
        
        Cursor.visible = false;
        PlayerController.Get().ChangeState<PlayerState_Moving>();
        Hide();
    }
}
