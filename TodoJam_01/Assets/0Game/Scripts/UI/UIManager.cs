﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UltimaCookie.Framework.Core;
using UnityEngine;

public class UIManager : UCMonoSingleton<UIManager>
{
    [SerializeField, Required]
    private UIFishingHUD _hudFishing;
    
    [SerializeField, Required]
    private UINewFishAcquiredDialog _dialogFishAcquired;
    
    [SerializeField, Required]
    private UIInventory _dialogInventory;

    [SerializeField, Required]
    private UIMovingHUD _panelMovingHud;

    [SerializeField, Required]
    private UIShrine _dialogShrine;

    [SerializeField, Required]
    private UICatchFish _dialogCatchFish;

    [SerializeField, Required]
    private UIIntro _dialogIntro;
    
    protected override void CustomInitialize()
    {
        _hudFishing.Initialize();
        _dialogFishAcquired.Initialize();
        _dialogIntro.Initialize();
        _dialogShrine.Initialize();
        _dialogCatchFish.Initialize();
        _dialogInventory.Initialize();
        _panelMovingHud.Initialize();

        ShowIntro();
    }

    public void ShowMovingHUD()
    {
        _panelMovingHud.Show();
    }
    
    public void ShowFishingUI()
    {
        _panelMovingHud.Hide();
        _hudFishing.Show();
    }
    
    public void ShowFishAcquired(FishObjectConfig fishObjectConfig)
    {
        _panelMovingHud.Hide();
        _dialogFishAcquired.Show(fishObjectConfig);
    }

    public void ShowInventory()
    {
        _panelMovingHud.Hide();
        _dialogShrine.Hide();
        _dialogInventory.Show();
    }

    public void ShowShrine()
    {
        _panelMovingHud.Hide();
        _dialogShrine.Show();
        _dialogInventory.Show();
    }

    public void ShowCatch()
    {
        _panelMovingHud.Hide();
        _dialogCatchFish.Show();
    }

    public void ShowIntro()
    {
        _panelMovingHud.Hide();
        _dialogIntro.Show();
    }
}
