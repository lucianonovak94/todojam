﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UltimaCookie.Framework.Events;
using Sirenix.OdinInspector;
using UnityEngine.UI;


public class ShowPondName : MonoBehaviour
{
    [SerializeField, Required]
    private TextMeshPro spotText;

    [SerializeField, Required]
    private FishingSpot fishingSpot;


    private void Start()
    {
        switch (fishingSpot.type)
        {
            case EFishPondType.CELEBRITIES:
                spotText.text = "V.I.P";
            break;
            case EFishPondType.GAMES:
                spotText.text = "PASION";
            break;
            case EFishPondType.RANDOM_STUFF:
                spotText.text = "MISTERIO";
            break;
            case EFishPondType.WAIFUS:
                spotText.text = "AMOR";
            break;
        }
    }
}
