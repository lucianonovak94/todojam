﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIInventoryDetailsPanel : MonoBehaviour
{
    [SerializeField, Required]
    private TextMeshProUGUI _textName;
    
    [SerializeField, Required]
    private TextMeshProUGUI _textDescription;
    
    [SerializeField, Required]
    private TextMeshProUGUI _textRarity;

    [SerializeField, Required]
    private Image _imgAvatar;
    
    private FishObject _fish;
    
    private FishConfigsScriptable _fishConfigs;

    public void Initialize()
    {
        _fishConfigs = Resources.Load<FishConfigsScriptable>("FishConfigsScriptable");
        gameObject.SetActive(false);
    }

    public void Show(FishObject fish)
    {
        _fish = fish;

        _textName.text = fish.Config.name;
        _textDescription.text = fish.Config.description;
        _textRarity.text = _fishConfigs.GetTextForRarity(fish.Config.rarity);
        _textRarity.color = _fishConfigs.GetColorForRarity(fish.Config.rarity);

        _imgAvatar.sprite = fish.Config.sprite;
        
        gameObject.SetActive(true);
    }

    public void HideFrom(FishObject fish)
    {
        if(fish != _fish)
            return;
        
        gameObject.SetActive(false);
        
    }
}
