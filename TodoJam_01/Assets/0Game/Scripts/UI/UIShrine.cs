﻿using System;
using System.Collections;
using System.Collections.Generic;
using UltimaCookie.Framework.Events;
using Sirenix.OdinInspector;
using UC.Managers;
using UnityEngine;
using UnityEngine.UI;

public class UIShrine : UIPanel
{
    public static UIShrine instance;
    
    [NonSerialized]
    public static CustomEventT<bool> onOferingDone = new CustomEventT<bool>();

    private bool offeringResult = false;
    public int itemsRequired = 5;

    [SerializeField, Required]
    private Button _btnOffer;

    [SerializeField, Required]
    private Button _btnConfirmConfirm;
    
    [SerializeField, Required]
    private Button _btnConfirmCancel;
    
    [SerializeField, Required]
    private UIInventorySlot _prefabInventorySlot;

    [SerializeField, Required]
    private Transform _tParentOffering;
    public Transform TParentOffering => _tParentOffering;

    [SerializeField, Required]
    private GameObject[] _hideWhenOffer;

    [SerializeField, Required]
    private GameObject _panelConfirmation;

    public override void Initialize()
    {
        instance = this;
        Hide();
        _btnOffer.onClick.AddListener(OnClick_Offer);
        _btnConfirmConfirm.onClick.AddListener(OnClick_ConfirmConfirmation);
        _btnConfirmCancel.onClick.AddListener(OnClick_CancelConfirmation);
        _panelConfirmation.SetActive(false);
    }

    protected override void OnDisableRuntime()
    {
        Hide();   
    }

    protected override void OnUpdate()
    {
        _btnOffer.interactable = _tParentOffering.childCount == 5;
    }

    protected override void BeforeShow()
    {
        UCAnalytics.PlayfabLogEvent("SHRINE_OPEN");
        
        _panelConfirmation.SetActive(false);
        
        foreach (Transform t in _tParentOffering)
        {
            Destroy(t.gameObject);
        }

        Cursor.visible = true;
    }
    
    private void OnClick_Offer()
    {
        UCAnalytics.PlayfabLogEvent("CLICK_START_OFFER");
        
        foreach (var objectsToHide in _hideWhenOffer)
        {
            objectsToHide.SetActive(false);
        }
        
        _panelConfirmation.SetActive(true);
    }

    private void OnClick_CancelConfirmation()
    {
        UCAnalytics.PlayfabLogEvent("CLICK_CANCEL_OFFER");
        
        foreach (var objectsToHide in _hideWhenOffer)
        {
            objectsToHide.SetActive(true);
        }
        
        _panelConfirmation.SetActive(false);
    }

    private void OnClick_ConfirmConfirmation()
    {
        UCAnalytics.PlayfabLogEvent("CLICK_CONFIRM_OFFER");
        
        FishingObjectManager.Get().ConfirmOffering();
    }
}