﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class UIFishingHUD : UIPanel
{
    [SerializeField, Required]
    private FishingSlide _sliderHUD; 
    
    public override void Initialize()
    {
        Hide();
    }

    protected override void BeforeShow()
    {
        _sliderHUD.Show();
    }
}
