﻿using System;
using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;
using SRF;
using UltimaCookie.Framework.Core;
using UltimaCookie.Framework.Events;

[ExecuteInEditMode]
public class FishingTransitionBlit : UCMonoSingleton<FishingTransitionBlit>
{
    public static readonly CustomEvent onTransitionFinished = new CustomEvent();
    
    [SerializeField, Required]
    private Material _transitionMaterial;

    public Texture[] transitionTextures;

    private void Start()
    {
        _transitionMaterial.SetFloat("_Fade", 0);
    }

    public void StartTransition()
    {
        _transitionMaterial.SetFloat("_Cutoff", 0);
        _transitionMaterial.SetFloat("_Fade", 1);

        _transitionMaterial.SetTexture("_TransitionTex", transitionTextures.Random());

        StartCoroutine(TransitionRoutine());
        
        IEnumerator TransitionRoutine()
        {
            float t = 0;
            
            float enterTime = 1f;


            while (t < enterTime)
            {
                t += Time.deltaTime;
                float ratio = t / enterTime;
                
                _transitionMaterial.SetFloat("_Cutoff", ratio);
                
                yield return null;
            }

            yield return new WaitForSeconds(0.3f);

            onTransitionFinished.Dispatch();
            
            float exitTime = 0.25f;
            t = 0;
            
            while (t < exitTime)
            {
                t += Time.deltaTime;
                float ratio = t / exitTime;
                
                _transitionMaterial.SetFloat("_Fade", ratio);
                
                yield return null;
            }
            
            _transitionMaterial.SetFloat("_Fade", 0);
        }
    }

    private void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
        if (_transitionMaterial != null)
            Graphics.Blit(src, dst, _transitionMaterial);
    }
    
    
}
