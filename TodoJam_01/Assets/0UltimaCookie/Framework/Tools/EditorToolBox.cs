using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace UltimaCookie.Framework.Editor
{
#if UNITY_EDITOR    
    public static class EditorToolBox
    {
        public static List<T> GetAllAssetsOfType<T>(string path) where T : Object
        {
            string[] guids = AssetDatabase.FindAssets($"t:{typeof(T).Name}", new string[] { path });
            
            List<T> assets = new List<T>();

            foreach (var guid in guids)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(guid);
                T asset = AssetDatabase.LoadAssetAtPath<T>(assetPath);
                
                assets.Add(asset);
            }
            
            return assets;
        }
    }
#endif    
}