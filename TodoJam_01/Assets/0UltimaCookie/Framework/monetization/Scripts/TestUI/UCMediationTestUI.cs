﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace UC.Monetization
{
    public class UCMediationTestUI : MonoBehaviour
    {
        [SerializeField, Required]
        private Button _btnShowBanner;
        
        [SerializeField, Required]
        private Button _btnHideBanner;
        
        [SerializeField, Required]
        private Button _btnShowVideoAd;
        
        [SerializeField, Required]
        private Button _btnShowRewarded;
        
        private void Start()
        {
            UCAdsManager.Get().Initialize();
            
            _btnShowBanner.onClick.AddListener(() =>
            {
                UCAdsManager.Get().ShowBanner();
            });
            
            _btnHideBanner.onClick.AddListener(() =>
            {
                UCAdsManager.Get().HideBanner();
            });
            
            _btnShowVideoAd.onClick.AddListener(() =>
            {
                UCAdsManager.Get().ShowVideoAd();
            });

            _btnShowRewarded.onClick.AddListener(() =>
            {
                UCAdsManager.Get().ShowRewarded((success) => { });
            });
        }
    }
}