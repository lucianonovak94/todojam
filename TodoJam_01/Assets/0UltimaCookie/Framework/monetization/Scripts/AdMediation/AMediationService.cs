﻿using System;
using UltimaCookie.Framework.Events;

namespace UC.Monetization
{
    public abstract class AMediationService
    {
        public CustomEventT<bool> onRewardedFinished = new CustomEventT<bool>();

        protected MonetizationConfig _config;

        protected bool _hasConsent;

        public void Initialize(bool hasConsent, MonetizationConfig config)
        {
            _config = config;
            _hasConsent = hasConsent;
            
            CustomInitialize();
        }

        protected abstract void CustomInitialize();

        public abstract bool IsLoaded_Rewarded();
        
        public abstract bool IsLoaded_CommonVideo();

        public abstract void LoadVideos();
        
        public abstract void ShowBanner();
        
        public abstract void HideBanner();
        
        public abstract void ShowVideoAd();
        
        public abstract void ShowRewarded(Action<bool> onShown);
    }
}