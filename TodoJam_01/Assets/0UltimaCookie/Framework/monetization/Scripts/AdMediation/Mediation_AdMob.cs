﻿// using System;
// using GoogleMobileAds.Api;
// using UC.Managers;
// using UnityEngine;
//
// namespace UC.Monetization
// {
//     public class Mediation_AdMob : AMediationService
//     {
//         private const string TEST_UNIT_ID_BANNER = "ca-app-pub-3940256099942544/6300978111";
//         private const string TEST_UNIT_ID_INTERSTITIAL = "ca-app-pub-3940256099942544/1033173712";
//         private const string TEST_UNIT_ID_REWARDED = "ca-app-pub-3940256099942544/5224354917";
//         private const string TEST_UNIT_ID_NATIVE = "ca-app-pub-3940256099942544/2247696110";
//         
//         private BannerView _bannerView;
//         private InterstitialAd _interstitial;
//         private RewardedAd _rewardedVideo;
//
//         private Action<bool> _rewardedCallback = null;
//         
//         protected override void CustomInitialize()
//         {
//             MobileAds.Initialize(initStatus =>
//             {
//                 // foreach (var kvp in initStatus.getAdapterStatusMap())
//                 // {
//                 //     Debug.Log(kvp.Key + " -> " + kvp.Value.Description);   
//                 // }
//             });
//             
//             RequestBanner();    
//             RequestInterstitial();
//             RequestRewarded();
//             Debug.Log("[Mediation_AdMob] Initialized");
//         }
//
//         public override bool IsLoaded_Rewarded()
//         {
//             return _rewardedVideo.IsLoaded();
//         }
//
//         public override bool IsLoaded_CommonVideo()
//         {
//             return _interstitial.IsLoaded();
//         }
//
//         public override void LoadVideos()
//         {
//             RequestBanner();
//             RequestInterstitial();
//             RequestRewarded();
//         }
//
//         private void RequestBanner()
//         {
//             string unitID = UCConfigs.Get().Config.debugMode ? TEST_UNIT_ID_BANNER :
//                             _config.BANNER_UNIT_ID;
//             
//             _bannerView = new BannerView(unitID, AdSize.Banner, AdPosition.Bottom);
//             AdRequest request = new AdRequest.Builder().Build();
//             _bannerView.LoadAd(request);
//
//             _bannerView.OnAdLoaded += (sender, args) => { Debug.Log("[Mediation_AdMob] Banner Loaded"); };
//             _bannerView.OnAdFailedToLoad  += (sender, args) =>
//             {
//                 Debug.LogWarning("[Mediation_AdMob] Banner FAILED to Load. Message =" + args.Message);
//             };
//         }
//
//         private void RequestInterstitial()
//         {
//             string unitID = UCConfigs.Get().Config.debugMode ? TEST_UNIT_ID_INTERSTITIAL : 
//                             _config.INTERSTITIAL_UNIT_ID;
//             
//             _interstitial = new InterstitialAd(unitID);
//
//             AdRequest request = new AdRequest.Builder().Build();
//             _interstitial.LoadAd(request);
//         }
//
//         private void RequestRewarded()
//         {
//             string unitID = UCConfigs.Get().Config.debugMode ? TEST_UNIT_ID_REWARDED : 
//                 _config.REWARDED_UNIT_ID;
//             
//             _rewardedVideo = new RewardedAd(unitID);
//
//             AdRequest request = new AdRequest.Builder().Build();
//             _rewardedVideo.LoadAd(request);
//
//             _rewardedVideo.OnAdLoaded += (sender, args) => { Debug.Log("[Mediation_AdMob] Rewarded Loaded");  };
//             _rewardedVideo.OnAdFailedToLoad  += (sender, args) =>
//             {
//                 Debug.LogWarning("[Mediation_AdMob] Rewarded FAILED to Load. Message =" + args.Message);
//             };
//             
//             _rewardedVideo.OnAdFailedToShow  += (sender, args) =>
//             {
//                 Debug.LogWarning("[Mediation_AdMob] Rewarded FAILED to Load. Message =" + args.Message);
//                 _rewardedCallback?.Invoke(false);
//                 _rewardedCallback = null;
//             };
//             
//             _rewardedVideo.OnAdClosed += (sender, args) =>
//             {
//                 Debug.Log("[Mediation_AdMob] Rewarded Shown");
//                 
//                 Debug.Log("[UCAdsManager] Rewarded Shown");
//                 _rewardedCallback?.Invoke(true);
//                 _rewardedCallback = null;
//             };
//
//             _rewardedVideo.OnAdOpening += (sender, args) =>
//             {
//                 Debug.Log("[Mediation_AdMob] Opening Rewarded");
//             };
//
//         }
//
//         public override void ShowBanner()
//         {
//             Debug.Log("[Mediation_AdMob] Show Banner");
//             _bannerView.Show();
//         }
//
//         public override void HideBanner()
//         {
//             Debug.Log("[Mediation_AdMob] Hider Banner");
//             _bannerView.Hide();
//         }
//
//         public override void ShowVideoAd()
//         {
//             if (_interstitial.IsLoaded())
//             {
//                 Debug.Log("[Mediation_AdMob] Show Interstitial");
//                 
//                 _interstitial.Show();
//             }
//             else
//             {
//                 AdRequest request = new AdRequest.Builder().Build();
//                 _interstitial.LoadAd(request);
//             }
//         }
//
//         public override void ShowRewarded(Action<bool> onShown)
//         {
//             _rewardedCallback = onShown;
//             _rewardedVideo.Show();
//         }
//     }
// }