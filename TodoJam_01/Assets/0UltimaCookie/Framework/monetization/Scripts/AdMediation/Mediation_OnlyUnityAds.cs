﻿using System;
using UC.Managers;
using UnityEngine;
using UnityEngine.Advertisements;

namespace UC.Monetization
{
    public class Mediation_OnlyUnityAds : AMediationService, IUnityAdsListener 
    {
        private const string APP_ID = "3654405";

        private Action<bool> _rewardedCallback = null;

        private bool _isReadyRewarded = false;
        
        private bool _isReadyVideo = false;
        
        protected override void CustomInitialize()
        {
            Advertisement.Initialize(APP_ID, UCConfigs.Get().Config.debugMode);

            Advertisement.Banner.Load("banner");
            
            Advertisement.AddListener(this);
        }

        public override bool IsLoaded_Rewarded()
        {
            return Advertisement.IsReady("rewardedVideo") && _isReadyRewarded;
        }
        
        public override bool IsLoaded_CommonVideo()
        {
            return Advertisement.IsReady("video") && _isReadyVideo;
        }

        public override void LoadVideos()
        {
            Advertisement.Load("rewardedVideo");
            Advertisement.Load("video");
        }

        public override void ShowBanner()
        {
            Debug.Log("[Mediation_OnlyUnityAds] Show Banner");
            
            Advertisement.Banner.SetPosition(BannerPosition.TOP_CENTER);
            Advertisement.Banner.Show("banner");
        }

        public override void HideBanner()
        {
            Debug.Log("[Mediation_OnlyUnityAds] Hider Banner");
            Advertisement.Banner.Hide();
        }

        public override void ShowVideoAd()
        {
            Advertisement.Show("video");
        }

        public override void ShowRewarded(Action<bool> onShown)
        {
            _rewardedCallback = onShown;
            Advertisement.Show("rewardedVideo");
        }

        public void OnUnityAdsReady(string placementId)
        {
            if (placementId == "rewardedVideo")
            {
                _isReadyRewarded = true;
            }
            else if (placementId == "video")
            {
                _isReadyVideo = true;
            }
        }

        public void OnUnityAdsDidError(string message)
        {
        }

        public void OnUnityAdsDidStart(string placementId)
        {
        }

        public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
        {
            if (placementId == "rewardedVideo")
            {
                _rewardedCallback?.Invoke(showResult == ShowResult.Finished);
            }
        }
    }
}