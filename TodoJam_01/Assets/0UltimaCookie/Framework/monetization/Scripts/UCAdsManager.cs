﻿using System;
using UC.Managers;
using UltimaCookie.Framework.Core;
using UnityEngine;

namespace UC.Monetization
{
    public class UCAdsManager : SimpleSingleton<UCAdsManager>
    {
        private AMediationService _mediationService;

        public void Initialize()
        {
            _mediationService = new Mediation_OnlyUnityAds();

#if UNITY_ANDROID
            _mediationService.Initialize(true, UCConfigs.Get().Config.monetizationAndroid);
#endif            

#if UNITY_IOS
            _mediationService.Initialize(true, UCConfigs.Get().Config.monetizationIOS);
#endif      
        }

        public bool AreRewardedReady() => _mediationService.IsLoaded_Rewarded();

        public void LoadVideos()
        {
            _mediationService.LoadVideos();
        }

        public void ShowBanner()
        {
            _mediationService.ShowBanner();
        }

        public void HideBanner()
        {
            _mediationService.HideBanner();
        }

        public void ShowVideoAd()
        {
            _mediationService.ShowVideoAd();    
        }

        public void ShowRewarded(Action<bool> onShown)
        {
#if UNITY_EDITOR
            onShown.Invoke(true);
            return;
#endif
            Debug.Log("[UCAdsManager] Will try Show Rewarded");
            _mediationService.ShowRewarded(onShown);
        }
    }
}