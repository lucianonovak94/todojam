﻿using System;
using System.IO;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using UC.Managers;
using UltimaCookie.Framework.Core;
using UnityEditor;
using UnityEngine;

namespace UC.Managers
{
    public class UCConfigs : UCMonoSingleton<UCConfigs>
    {
        public static string generatedPath() => Application.dataPath + "/0UltimaCookie/generated";
        public static string configFilePath() => generatedPath() + "/Resources/ucGeneralConfigs.json";
        
        private GeneralOptionsData config;

        public GeneralOptionsData Config
        {
            get
            {
                if(config == null)
                    Initialize();

                return config;
            }
        }

        private void Initialize()
        {
            TextAsset json = Resources.Load<TextAsset>("ucGeneralConfigs");
            config = JsonConvert.DeserializeObject<GeneralOptionsData>(json.text);
        }
    }
}

[System.Serializable]
public class GeneralOptionsData
{
    [BoxGroup("Auto Run Scene")]
    [OnValueChanged("Save")]
    public bool autoRunGivenScene = true;
    
    [DisableIf("@!autoRunGivenScene")]
    [BoxGroup("Auto Run Scene")]
    public string sceneToBoot = "Boot";
    
    [DisableIf("@!autoRunGivenScene")]
    [BoxGroup("Auto Run Scene")]
    public string sceneAfterBoot = "Game";

    [BoxGroup("Build")]
    [OnValueChanged("Save")]
    public bool debugMode = true;

    [BoxGroup("Settings")]
    public string playfabID = "";

    [BoxGroup("Monetization")]
    public MonetizationConfig monetizationAndroid;
    
    [BoxGroup("Monetization")]
    public MonetizationConfig monetizationIOS;

#if UNITY_EDITOR    
    [Button("Save")]
    public void Save()
    {
        File.WriteAllText(UCConfigs.configFilePath(), JsonConvert.SerializeObject(this, Formatting.Indented));
        UnityEditor.AssetDatabase.Refresh();

        if (autoRunGivenScene)
        {
            EditorPrefs.SetString("AutoRunScene", sceneToBoot);
        }
        else
        {
            EditorPrefs.DeleteKey("AutoRunScene");
        }
    }
#endif    
}

[Serializable]
public class MonetizationConfig
{
    public string INTERSTITIAL_UNIT_ID = "";
    public string BANNER_UNIT_ID = "";
    public string REWARDED_UNIT_ID = "";
}