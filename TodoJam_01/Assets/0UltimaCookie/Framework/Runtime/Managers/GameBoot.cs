﻿using System;
using System.Collections;
using PlayFab;
using PlayFab.ClientModels;
using UC.Monetization;
using UltimaCookie.Framework.Core;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UC.Managers
{
    public class GameBoot : UCMonoSingleton<GameBoot>
    {
        private void Start()
        {
            DontDestroyOnLoad(gameObject);

            if (UCConfigs.Get().Config.debugMode)
            {
                SRDebug.Init();
            }

            UCAdsManager.Get().Initialize();

            string playfabID = UCConfigs.Get().Config.playfabID;

            PlayFabSettings.staticSettings.TitleId = playfabID;

            string localPlayerID = PlayerPrefs.GetString("PGUI", SystemInfo.deviceUniqueIdentifier);
            
            ChatConfig chatConfig = Resources.Load<ChatConfig>("ChatConfig");

            if (!chatConfig.isClient)
            {
                localPlayerID = "GodServer";
            }

            LoginWithCustomIDRequest loginRequest = new LoginWithCustomIDRequest();
            loginRequest.CustomId = localPlayerID;
            loginRequest.CreateAccount = true;

            PlayFabClientAPI.LoginWithCustomID(loginRequest, result =>
                {
                    BackendManager.playfabID = result.PlayFabId;
                    playfabID = result.PlayFabId;
                    PlayerPrefs.SetString("PGUI", localPlayerID);
                },
                error =>
                {
                    Debug.LogError("[Playfab] Login Error. = " + error.ErrorMessage);
                });

            StartCoroutine(StartGameScene());
        }

        private IEnumerator StartGameScene()
        {
#if !UNITY_EDITOR
        yield return new WaitForSeconds(3f);
#endif
            SceneManager.LoadScene(UCConfigs.Get().Config.sceneAfterBoot);

            yield break;
        }
    }
}