﻿using UltimaCookie.Framework.Core;
using UnityEngine.Events;

public class UCSplashScreen : UCMonoSingleton<UCSplashScreen>
{
    public static readonly UnityEvent doHide = new UnityEvent();
    
    void Start()
    {
        DontDestroyOnLoad(gameObject);
        
        doHide.AddListener(Hide);
        
    }

    private void Hide()
    {
        doHide.RemoveAllListeners();
        Destroy(gameObject);
    }
}
