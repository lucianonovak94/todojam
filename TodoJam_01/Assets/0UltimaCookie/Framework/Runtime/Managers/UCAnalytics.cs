﻿using System.Collections.Generic;
using GameAnalyticsSDK;
using PlayFab;
using PlayFab.ClientModels;
using UltimaCookie.Framework.Core;

namespace UC.Managers
{
    public class UCAnalytics : UCMonoSingleton<UCAnalytics>
    {
        public static bool IS_ACTIVE = true;
        
        // Start is called before the first frame update
        void Start()
        {
            GameAnalytics.Initialize();
        }


        public static void PlayfabLogEvent(string eventName, Dictionary<string, object> data = null)
        {
            if (!PlayFabClientAPI.IsClientLoggedIn() || !IS_ACTIVE)
                return;

            WriteClientPlayerEventRequest eventRequest = new WriteClientPlayerEventRequest();
            eventRequest.EventName = eventName;
            eventRequest.Body = data;

            PlayFabClientAPI.WritePlayerEvent(
                eventRequest,
                response => { },
                error => { });
        }
    }
}