using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace UltimaCookie.Framework.Misc
{
    public static class ToolBox
    {
        public static T[] GetComponentsInHierarchy<T>(GameObject parentGameObject)
        {
            var inObject = parentGameObject.GetComponents<T>();
            var inChildren = parentGameObject.GetComponentsInChildren<T>(true);
            
            T[] result = new T[inObject.Length + inChildren.Length];
            Array.Copy(inObject, 0, result, 0, inObject.Length);
            Array.Copy(inChildren, 0, result, inObject.Length, inChildren.Length);

            return result;
        }

        public static T GetUniqueResourceOfType<T>() where T : Object
        {
            T[] resources = Resources.LoadAll<T>("");

            if (resources.Length == 0)
                throw new Exception($"Resources of type {typeof(T)} not found");
            
            if (resources.Length > 1)
                throw new Exception($"Resources of type {typeof(T)} must be Unique, but we found {resources.Length}");

            return resources[0];
        }

        public static bool StringIsIDFormat(string testString)
        {

            string IDREGEX = "abcdefghijklmnopqrstuvwzyx0123456789_";

            foreach (char c in testString)
            {
                if (IDREGEX.IndexOf(c) < 0)
                    return false;
            }
            
            return true;
        }

        public static Bounds GetBoundsForGameObject(GameObject go, bool throwIfNoRenderers = true)
        {
            List<Renderer> renderers = new List<Renderer>();

            Renderer goRenderer = go.GetComponent<Renderer>();

            if (goRenderer != null)
            {
                renderers.Add(goRenderer);
            }
            
            renderers.AddRange(go.transform.GetComponentsInChildren<Renderer>());

            if (renderers.Count == 0)
            {
                if(throwIfNoRenderers)
                    throw new Exception("No Renderers Founds");
                else
                    return new Bounds();
            }
            
            Bounds combinedBounds = renderers[0].bounds;

            foreach (var renderer in renderers)
            {
                combinedBounds.Encapsulate(renderer.bounds);
            }
            
            return combinedBounds;
        }

        public static Bounds GetBoundsForRenderers(Renderer[] allRenderers)
        {
            Bounds combinedBounds = allRenderers[0].bounds;
            
            for(int i = 0; i < allRenderers.Length; i++)
            {
                combinedBounds.Encapsulate(allRenderers[i].bounds);
            }
            
            return combinedBounds;
        }

        public static void DestroyAllChildrenThatContainsInName(Transform transform, string substring)
        {
            foreach (Transform child in transform)
            {
                if (child.gameObject.name.Contains(substring))
                {
                    GameObject.Destroy(child.gameObject);
                }
            }
        }
        
        public static void DestroyAllChildrenFromTransform(Transform transform)
        {
            foreach (Transform child in transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }

        public static string GetRandomID(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            string id = "";

            for (int i = 0; i < length; i++)
            {
                id += chars[ Random.Range(0, chars.Length)];
            }

            return id;
        }

        public static Vector3 GetVector3FromSaveString(string saveString)
        {
            string[] components = saveString.Split(',');
            
            Vector3 returnVector = new Vector3();

            returnVector.x = float.Parse(components[0]);
            returnVector.y = float.Parse(components[1]);
            returnVector.z = float.Parse(components[2]);
            
            return returnVector;
        }

        public static string GetSaveStringFromVector3(Vector3 v3)
        {
            string saveString = "";

            saveString += v3.x.ToString("F2") + ",";
            saveString += v3.y.ToString("F2") + ",";
            saveString += v3.z.ToString("F2");
            
            return saveString;
        }

        public static double GetCurrentTimeInSeconds()
        {
            DateTime dt = DateTime.Now;
            TimeSpan ts = new TimeSpan(dt.Ticks);

            return ts.TotalSeconds;
        }

        public static void ApplyMaterialToAllSubMeshes(MeshRenderer[] meshRenderers, Material newMaterial)
        {
            foreach (var meshRenderer in meshRenderers)
            {
                Material[] mats = new Material[meshRenderer.materials.Length];
                
                
                for (int i = 0; i < mats.Length; i++)
                {
                    // this destroy is important to avoid memory leaks!
                    GameObject.Destroy(meshRenderer.materials[i]);
                    mats[i] = newMaterial;
                }

                meshRenderer.materials = mats;
            }
        }
    }
}