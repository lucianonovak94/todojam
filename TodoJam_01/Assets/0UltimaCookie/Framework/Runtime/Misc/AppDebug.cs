﻿using UnityEngine;

namespace UltimaCookie.Framework.Misc
{
    public class AppDebug
    {
        public static void AssertNotNull(object objectToCheck, MonoBehaviour parentMonoBehaviour, string errorMessage = "")
        {
            if (objectToCheck == null)
            {
                Debug.LogError($"({parentMonoBehaviour.gameObject.name}) {errorMessage}", parentMonoBehaviour.gameObject);
            }
        }
        
        public static void AssertNotNull(object objectToCheck, GameObject parentGameObject, string errorMessage = "")
        {
            if (objectToCheck == null)
            {
                Debug.LogError($"({parentGameObject.gameObject.name}) {errorMessage}", parentGameObject.gameObject);
            }
        }
    }
}