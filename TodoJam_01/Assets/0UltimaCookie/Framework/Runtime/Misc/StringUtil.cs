using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Random = System.Random;

namespace UltimaCookie.Framework.Misc
{
    public static class StringUtil
    {
        private static Random random = new Random();

        /// <summary>
        /// Returns the supplied Objects properties as a string formatted {propertyName : value }
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToString(object obj)
        {
            var output = "[" + obj.GetType().Name + "] - ";
            var properties = obj.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);
            var firstProp = true;

            // Best method: uses the properties discovered using reflection
            if (properties.Length > 0)
            {
                foreach (var prop in properties)
                {
                    if (!firstProp)
                    {
                        output += ", ";
                    }

                    output += prop.Name + ": " + prop.GetValue(obj);

                    firstProp = false;
                }
            }

            // fallback solution: some objects cannot discover properties by reflection
            else
            {
                try
                {
                    var objTypes = ((IEnumerable) obj).Cast<object>().Select(x => x.GetType().ToString()).ToArray();
                    var objValues = ((IEnumerable) obj).Cast<object>().Select(x => x.ToString()).ToArray();

                    for (var i = 0; i < objTypes.Length; i++)
                    {
                        if (!firstProp)
                        {
                            output += ", ";
                        }

                        output += objTypes[i] + ": " + objValues[i];

                        firstProp = false;
                    }
                }
                catch
                {
                    // and some types just dont wanna, like Streams for example
                    output = "Unable to use reflection or object casting on this object!";
                }

            }

            return output;
        }

        /// <summary>
        /// Returns the supplied Dictionary as a string formatted {item.Key : value }, for each item in the Dictionary
        /// </summary>
        /// <param name="dictionary"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <returns></returns>
        public static string ToString<TKey, TValue>(Dictionary<TKey, TValue> dictionary)
        {
            var output = "[Dictionary] - ";
            var firstProp = true;

            foreach (var item in dictionary)
            {
                if (!firstProp)
                {
                    output += ", ";
                }

                output += "[ " + item.Key + ": " + item.Value + " ]";

                firstProp = false;
            }

            return output;
        }

        /// <summary>
        /// Returns a randomString of the supplied length
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        /// <summary>
        /// Checks if a string is encoded with base64
        /// </summary>
        /// <param name="stringToCheck"></param>
        /// <returns></returns>
        public static bool IsBase64String(string stringToCheck)
        {
            if (string.IsNullOrWhiteSpace(stringToCheck))
            {
                return false;
            }

            stringToCheck = stringToCheck.Trim();
            return (stringToCheck.Length % 4 == 0) &&
                   Regex.IsMatch(stringToCheck, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None);
        }
    }
}