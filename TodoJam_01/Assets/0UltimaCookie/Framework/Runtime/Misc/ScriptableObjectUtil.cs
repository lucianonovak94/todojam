using UnityEngine;

namespace UltimaCookie.Framework.Misc
{
    /// <summary>
    /// Various Utils relating to Scriptable Objects
    /// </summary>
    public static class ScriptableObjectUtil
    {
        public static void Save(ScriptableObject so)
        {
            #if UNITY_EDITOR
            UnityEditor.AssetDatabase.Refresh();
            UnityEditor.EditorUtility.SetDirty(so);
            UnityEditor.AssetDatabase.SaveAssets();
            #endif
        }
    }
}