using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace UltimaCookie.Framework.Misc
{
    public static class UCMath
    {
        public static int Fact(int n)
        {
            int val = 1;

            for (int i = 2; i <= n; i++)
            {
                val *= i;
            }

            return val;
        }
        
        public static int SumChain(int n, int m)
        {
            int val = 0;

            for (int i = n; i <= m; i++)
            {
                val += i;
            }

            return val;
        }
    }
}