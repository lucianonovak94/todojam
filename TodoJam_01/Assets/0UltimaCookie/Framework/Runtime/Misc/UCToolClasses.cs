﻿using UnityEngine;

namespace UltimaCookie.Framework.Misc
{
    [System.Serializable]
    public struct MinMaxFloat
    {
        public float Min;
        public float Max;

        public MinMaxFloat(float min, float max)
        {
            Min = min;
            Max = max;
        }

        public float GetRandomInRange()
        {
            return Random.Range(Min, Max);
        }
    }
    
    [System.Serializable]
    public struct MinMaxInt
    {
        public int Min;
        public int Max;

        public MinMaxInt(int min, int max)
        {
            Min = min;
            Max = max;
        }
        public int GetRandomInRange()
        {
            return Random.Range(Min, Max);
        }
    }
}