﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine.Events;

namespace UltimaCookie.Framework.Events
{ 
    public class OneTimeEvent : AbstractCustomEvent
    {
        private readonly List<Action> _allActions = new List<Action>();
        private readonly HashSet<Action> _mapActions = new HashSet<Action>();

        private bool _triggered = false;
        
        public OneTimeEvent()
        {
            _allActions = new List<Action>();
            _mapActions = new HashSet<Action>();
        }

        public void AddOneTimeCallback(Action newCallback)
        {
            if (_triggered)
            {
                newCallback.Invoke();
                return;
            }

            if (newCallback == null || _mapActions.Contains(newCallback))
                return;

            _mapActions.Add(newCallback);
            _allActions.Add(newCallback);
        }
        
        public void RemoveListener(Action newCallback)
        {
            if (newCallback == null || !_mapActions.Contains(newCallback))
                return;

            _mapActions.Remove(newCallback);
            _allActions.Remove(newCallback);
        }

        [Button]
        public void Dispatch()
        {
            _triggered = true;
            
            for (int i = 0; i < _allActions.Count; i++)
            {
                _allActions[i]();
            }
            
            RemoveAllListeners();
        }

        public override void RemoveAllListeners()
        {
            _allActions.Clear();
            _mapActions.Clear();
        }
    }
}