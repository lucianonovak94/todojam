﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace UltimaCookie.Framework.Events
{
    [Serializable]
    public class UnityEvent_Renderer : UnityEvent<Renderer> { }
    
    [Serializable]
    public class UnityEvent_GameObject : UnityEvent<GameObject> { }
}