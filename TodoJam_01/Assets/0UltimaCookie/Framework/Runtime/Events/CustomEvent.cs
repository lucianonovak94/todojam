﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine.Events;

namespace UltimaCookie.Framework.Events
{ 
    public class CustomEvent : AbstractCustomEvent
    {
        private readonly List<Action> allActions = new List<Action>();
        private readonly HashSet<Action> mapActions = new HashSet<Action>();

        public CustomEvent()
        {
            allActions = new List<Action>();
            mapActions = new HashSet<Action>();
        }

        public void AddListener(Action newCallback)
        {
            if (newCallback == null || mapActions.Contains(newCallback))
                return;

            mapActions.Add(newCallback);
            allActions.Add(newCallback);
        }
        
        public void RemoveListener(Action newCallback)
        {
            if (newCallback == null || !mapActions.Contains(newCallback))
                return;

            mapActions.Remove(newCallback);
            allActions.Remove(newCallback);
        }

        [Button]
        public void Dispatch()
        {
            for (int i = 0; i < allActions.Count; i++)
            {
                allActions[i]();
            }
        }

        public override void RemoveAllListeners()
        {
            allActions.Clear();
            mapActions.Clear();
        }
    }
}