namespace UltimaCookie.Framework.Events
{
    
    public abstract class AbstractCustomEvent
    {
        public abstract void RemoveAllListeners();
    }
}