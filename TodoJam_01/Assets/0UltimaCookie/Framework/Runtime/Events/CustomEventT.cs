﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace UltimaCookie.Framework.Events
{
    public class CustomEventT<T> : AbstractCustomEvent
    {
        private readonly List<Action<T>> _allActions = new List<Action<T>>();
        private readonly HashSet<Action<T>> _mapActions = new HashSet<Action<T>>();
        
        public int QtyDelegates => _allActions.Count;

        public void AddListener(Action<T> newCallback)
        {
            if (newCallback == null || _mapActions.Contains(newCallback))
                return;

            _mapActions.Add(newCallback);
            _allActions.Add(newCallback);
        }
        
        public void RemoveListener(Action<T> newCallback)
        {
            if (newCallback == null || !_mapActions.Contains(newCallback))
                return;

            _mapActions.Remove(newCallback);
            _allActions.Remove(newCallback);
        }

        [Button]
        public void Dispatch(T param)
        {
            for (int i = 0; i < QtyDelegates; i++)
            {
                _allActions[i](param);
            }
        }

        public override void RemoveAllListeners()
        {
            _allActions.Clear();
            _mapActions.Clear();
        }
    }
}