﻿using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

namespace UltimaCookie.Framework.Numeric
{
    public class BigDoubleDrawer : OdinValueDrawer<BigDouble>
    {
#pragma warning disable CS0672        
        protected override void DrawPropertyLayout(IPropertyValueEntry<BigDouble> entry, GUIContent label)
        {
            EditorGUI.BeginChangeCheck();
    
            EditorGUILayout.BeginHorizontal();
    
            EditorGUILayout.LabelField(entry.Property.Name, GUILayout.Width(100));
            
            BigDouble value = BigDoubleTools.FromSaveString(EditorGUILayout.TextField(entry.SmartValue.ToSaveString()));

            if (EditorGUI.EndChangeCheck())
            {
                entry.SmartValue = value;
            }
    
            EditorGUILayout.EndHorizontal();
        }
#pragma warning enable CS0672         
    }
}