﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace UltimaCookie.Framework.Numeric
{
    public static class BigDoubleTools
    {
        public const int QTY_LETTERS = 26;

        public static List<string> moneyPrefixes;

        [RuntimeInitializeOnLoadMethod]
        public static void GenerateBigNumberPrefixsString()
        {
            string[] lettersFirst = new string[] {"", "K", "M", "B", "T"};
            string[] lettersAfter = new string[]
            {
                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
                "v", "w", "x", "y", "z"
            };

            moneyPrefixes = new List<string>();
            moneyPrefixes.AddRange(lettersFirst);

            int qtyLetters = lettersAfter.Length;

            for (int i = 0; i < qtyLetters; i++)
            {
                moneyPrefixes.Add(lettersAfter[i]);
            }

            for (int i = 0; i < qtyLetters; i++)
            {
                for (int j = 0; j < qtyLetters; j++)
                {
                    moneyPrefixes.Add(lettersAfter[i] + lettersAfter[j]);
                }
            }

            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < qtyLetters; j++)
                {
                    for (int k = 0; k < qtyLetters; k++)
                    {
                        moneyPrefixes.Add(lettersAfter[i] + lettersAfter[j] + lettersAfter[k]);
                    }
                }
            }
        }

        public static string To10kOrMoreString(this BigDouble bd)
        {
            if (bd < 10000)
            {
                return bd.ToScreenString(0);
            }

            return bd.ToScreenString(2);
        }

        public static string ToScreenString(this BigDouble bd, int decimalRounds = 2)
        {
            string screenString = bd.ToString("G8").ToLower();
            string[] parts = screenString.Split(new string[] {"e+"}, StringSplitOptions.None);

            if (parts.Length == 1)
            {
                double mant = double.Parse(parts[0]);
                int thousands = 0;
                if (mant > 9999)
                {
                    while (mant > 1000.0)
                    {
                        mant /= 1000.0;
                        thousands++;
                    }

                    return Math.Round(mant, decimalRounds).ToString() + moneyPrefixes[thousands];
                }
                else
                {
                    return Math.Round(mant, decimalRounds).ToString();
                }
            }

            long exponentE10 = long.Parse(parts[1]);
            long exponentE1000 = (exponentE10 / 3);
            long rest100 = exponentE10 % 3;
            double mantisa = double.Parse(parts[0]);
            mantisa *= Math.Pow(10, rest100);
            mantisa = Math.Round(mantisa, decimalRounds);

            string kLetter = moneyPrefixes[(int) exponentE1000];
            screenString = mantisa.ToString() + kLetter;
            return screenString;
        }

        public static BigDouble FromSaveString(string saveString)
        {

            BigDouble newNumber = new BigDouble();

            if (string.IsNullOrEmpty(saveString))
            {
                newNumber = 0;
                return newNumber;
            }

            saveString = saveString.Trim();

            saveString = saveString.ToUpper();

            // -- current save formatting
            if (saveString.Contains("E+"))
            {
                string[] parts = saveString.Split(new string[] {"E+"}, StringSplitOptions.None);
                newNumber = new BigDouble(double.Parse(parts[0]), long.Parse(parts[1]));
            }
            // -- support for old save formatting
            else if (saveString.Contains("T"))
            {
                string[] parts = saveString.Split('T');

                double mantisa = double.Parse(parts[0]);
                long exp = long.Parse(parts[1]) * 3;
                newNumber = new BigDouble(mantisa, exp);
            }
            else
            {
                newNumber = BigDouble.Parse(saveString);
            }

            return newNumber;
        }

        public static string ToSaveString(this BigDouble bd)
        {
            string saveSt = bd.ToString("G5").ToLower();
            return saveSt;
        }

        public static void ClampFloor(this BigDouble bd, double floor)
        {
            if (bd.Mantissa < floor)
                bd = floor;
        }

        public static long MaxPurchasableFormula(BigDouble costForLevel, BigDouble playerMoney,
            BigDouble incrementalBase)
        {
            BigDouble a = new BigDouble(incrementalBase);
            a -= 1;
            a *= playerMoney;
            a /= costForLevel;
            a += 1;

            BigDouble topLog = BigDouble.Log(a, Math.E);
            BigDouble botLog = BigDouble.Log(incrementalBase, Math.E);

            BigDouble returnValue = topLog / botLog;

            return (long) returnValue.ToDouble();
        }
        
        public static BigDouble GetBulkCost(BigDouble costForOne, BigDouble bulkQty, double incrementalBase)
        {
            if (bulkQty.Equals(1))
                return costForOne;

            BigDouble partA = BigDouble.Pow(incrementalBase, bulkQty) - 1;
            BigDouble partB = (incrementalBase - 1);
            BigDouble partC = partA / partB;
            BigDouble bulkCost = costForOne * partC;
        
            if (BigDouble.IsInfinity(bulkCost))
            {
                Debug.LogError("aca");
            }
        
            return bulkCost;
        }

        public static float Proportion(BigDouble top, BigDouble bot)
        {
            return (float) (top / bot).ToDouble();
        }

        public static bool CheckString(string stNumber)
        {
            if (stNumber.Length == 0)
                return false;

            string[] parts = stNumber.Split(new char[] {'T'}, StringSplitOptions.RemoveEmptyEntries);

            if (parts.Length == 2)
                return false;

            double outNumber;

            if (!double.TryParse(parts[0], out outNumber))
                return false;

            if (!double.TryParse(parts[1], out outNumber))
                return false;

            return true;
        }

    }
}