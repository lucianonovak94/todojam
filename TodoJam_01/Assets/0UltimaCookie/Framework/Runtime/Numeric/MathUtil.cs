using System;
using UnityEngine;
using Random = System.Random;

namespace UltimaCookie.Framework.Numeric
{
    public static class MathUtil
    {
        private static readonly Random random = new Random();

        public static float ClampAngle(float angle, float from, float to)
        {
            if (angle < 0f) angle = 360 + angle;
            if (angle > 180f) return Mathf.Max(angle, 360+from);
            return Mathf.Min(angle, to);
        }
        
        public static int GetRandomNumber(int min, int max)
        {
            return random.Next(min, max);
        }

        public static void RoundToDecimal(ref Vector2 source, int decimals)
        {
            source.Set((float) Math.Round(source.x, decimals), (float) Math.Round(source.y, decimals));
        }

        public static void RoundToDecimal(ref float source, int decimals)
        {
            source = (float) Math.Round(source, decimals);
        }

        public static void RoundToInteger(ref Vector2 source)
        {
            source.Set((float) Math.Round(source.x, 0), (float) Math.Round(source.y, 0));
        }

        /// <summary>
        /// Checks if 2 <see cref="Vector2"/> are equal, within the supplied tolerance value
        /// </summary>
        /// <param name="vector1"></param>
        /// <param name="vector2"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool CheckEquality(Vector2 vector1, Vector2 vector2, float tolerance = Vector2.kEpsilon)
        {
            return Math.Abs(vector1.x - vector2.x) <= tolerance && Math.Abs(vector1.y - vector2.y) <= tolerance;
        }

        /// <summary>
        /// Checks if 2 <see cref="Vector3"/> are equal, within the supplied tolerance value
        /// </summary>
        /// <param name="vector1"></param>
        /// <param name="vector2"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool CheckEquality(Vector3 vector1, Vector3 vector2, float tolerance = Vector3.kEpsilon)
        {
            return Math.Abs(vector1.x - vector2.x) <= tolerance &&
                   Math.Abs(vector1.y - vector2.y) <= tolerance &&
                   Math.Abs(vector1.z - vector2.z) <= tolerance;
        }

        /// <summary>
        /// Checks if 2 <see cref="float"/> are equal, within the supplied tolerance value
        /// </summary>
        /// <param name="float1"></param>
        /// <param name="float2"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool CheckEquality(float float1, float float2, float tolerance = float.Epsilon)
        {
            return Math.Abs(float1 - float2) <= tolerance;
        }

        /// <summary>
        /// Takes a value within a supplied range and outputs the equivalent value based on a targetRange
        /// </summary>
        /// <param name="value"></param>
        /// <param name="valueRangeMin"></param>
        /// <param name="valueRangeMax"></param>
        /// <param name="targetRangeMin"></param>
        /// <param name="targetRangeMax"></param>
        /// <returns></returns>
        public static float ConvertValueRange(float value, float valueRangeMin, float valueRangeMax,
            float targetRangeMin, float targetRangeMax)
        {
            return (value - valueRangeMin) / (valueRangeMax - valueRangeMin) * (targetRangeMax - targetRangeMin) +
                   targetRangeMin;
        }
    }
}