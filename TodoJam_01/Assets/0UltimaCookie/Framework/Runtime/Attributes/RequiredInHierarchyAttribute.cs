using System;

namespace UltimaCookie.Framework.Attributes
{
    public class RequiredInHierarchyAttribute : Attribute
    {
        public string[] IgnoreIfObjectsAreCalled = null;
        
        public RequiredInHierarchyAttribute()
        {
        }

        public RequiredInHierarchyAttribute(string[] ignoreIfObjectsAreCalled)
        {
            this.IgnoreIfObjectsAreCalled = ignoreIfObjectsAreCalled;
        }
    }
}