﻿using System;

namespace UltimaCookie.Framework.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class SealMethodAttribute : Attribute
    {
    }
}