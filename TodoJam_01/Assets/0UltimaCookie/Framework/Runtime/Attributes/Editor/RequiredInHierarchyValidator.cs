using System.Linq;
using System.Reflection;
using UltimaCookie.Framework.Attributes;
using Sirenix.OdinInspector.Editor.Validation;
using UnityEngine;

[assembly: RegisterValidator(typeof(RequiredInHierarchyValidator))]

namespace UltimaCookie.Framework.Attributes
{
    public class RequiredInHierarchyValidator : AttributeValidator<RequiredInHierarchyAttribute>
    {
        protected override void Validate(object parentInstance, object memberValue, MemberInfo member, ValidationResult result)
        {
            MonoBehaviour script = parentInstance as MonoBehaviour;

            string[] ignoreObjects = Attribute.IgnoreIfObjectsAreCalled;

            if (ignoreObjects != null)
            {
                if (ignoreObjects.Contains(script.gameObject.name))
                {
                    result.ResultType = ValidationResultType.Valid;
                    return;
                }
            }


            if (script.gameObject.scene.IsValid())
            {
                if (memberValue == null)
                {
                    result.ResultType = ValidationResultType.Error;
                    result.Message = $"<{member.Name}> can't be null in Hierarchy: <{script.gameObject.name}>";
                }
            }
        }
    }
}