using System.Reflection;
using UltimaCookie.Framework.Attributes;
using Sirenix.OdinInspector.Editor.Validation;
using UnityEngine;

[assembly: RegisterValidator(typeof(RequiredWhenPrefabValidator))]

namespace UltimaCookie.Framework.Attributes
{
    public class RequiredWhenPrefabValidator : AttributeValidator<RequiredWhenPrefabAttribute>
    {
        protected override void Validate(object parentInstance, object memberValue, MemberInfo member, ValidationResult result)
        {
            MonoBehaviour script = parentInstance as MonoBehaviour;

            if (!script.gameObject.scene.IsValid())
            {
                if (memberValue == null)
                {
                    result.ResultType = ValidationResultType.Error;
                    result.Message = $"<{member.Name}> can't be null in prefab: <{script.gameObject.name}>";
                }
            }
        }
    }
}