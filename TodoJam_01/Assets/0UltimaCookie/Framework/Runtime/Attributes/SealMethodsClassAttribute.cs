﻿using System;

namespace UltimaCookie.Framework.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class SealMethodsClassAttribute : Attribute
    {
    }
}