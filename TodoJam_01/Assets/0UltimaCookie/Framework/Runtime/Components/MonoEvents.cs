﻿using UltimaCookie.Framework.Events;
using UnityEngine;

namespace UltimaCookie.Framework.Components
{
    public class MonoEvents : MonoBehaviour
    {
        public readonly UnityEvent_GameObject onEnabledEvent = new UnityEvent_GameObject();
        
        public readonly UnityEvent_GameObject onDisabledEvent = new UnityEvent_GameObject();

        public readonly UnityEvent_GameObject onDestroyedEvent = new UnityEvent_GameObject();

        private void OnEnable()
        {
            onEnabledEvent.Invoke(gameObject);
        }

        private void OnDisable()
        {
            onDisabledEvent.Invoke(gameObject);
        }
        
        private void OnDestroy()
        {
            onDestroyedEvent.Invoke(gameObject);
        }
    }
}