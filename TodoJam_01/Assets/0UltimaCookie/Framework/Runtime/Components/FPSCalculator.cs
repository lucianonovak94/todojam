﻿using UnityEngine;
using System.Collections;
using UltimaCookie.Framework.Core;

namespace UltimaCookie.Framework.Components
{
    public class FPSCalculator : UCMonoSingleton<FPSCalculator>
    {

        private int FramesPerSec;
        private float frequency = 1.0f;
        public string FPSLabel;

        private void Start()
        {
            StartCoroutine(FPS());
        }

        private IEnumerator FPS()
        {
            for (;;)
            {
                // Capture frame-per-second
                int lastFrameCount = Time.frameCount;
                float lastTime = Time.realtimeSinceStartup;
                yield return new WaitForSeconds(frequency);
                float timeSpan = Time.realtimeSinceStartup - lastTime;
                int frameCount = Time.frameCount - lastFrameCount;

                // Display it
                FPSLabel = string.Format("FPS: {0}", Mathf.RoundToInt(frameCount / timeSpan));
            }
        }
    }
}