﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UltimaCookie.Framework.Components
{
    public class AutoDestroyMeshRenderer : MonoBehaviour
    {

        void Awake()
        {
            MeshRenderer mr = GetComponent<MeshRenderer>();
            MeshFilter mf = GetComponent<MeshFilter>();

            Destroy(mr);
            Destroy(mf);
        }
    }
}