﻿using System.Collections.Generic;
using UnityEngine;

namespace UltimaCookie.Framework.Components
{
    public class ColliderRegister : MonoBehaviour
    {
        public List<Collider> TouchingColliders = new List<Collider>();

        private void OnCollisionEnter(Collision other)
        {
            TouchingColliders.Add(other.collider);
        }
        
        private void OnCollisionExit(Collision other)
        {
            TouchingColliders.Remove(other.collider);
        }

        private void OnTriggerEnter(Collider other)
        {
            TouchingColliders.Add(other);
        }

        private void OnTriggerExit(Collider other)
        {
            TouchingColliders.Remove(other);
        }
    }
}