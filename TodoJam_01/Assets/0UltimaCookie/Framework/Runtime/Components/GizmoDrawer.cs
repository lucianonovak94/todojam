﻿using UnityEngine;

namespace UltimaCookie.Framework.Components
{
    public class GizmoDrawer : MonoBehaviour
    {
#if UNITY_EDITOR

        public float sphereRadius = 1f;
        
        private void OnDrawGizmos()
        {
            Gizmos.DrawSphere(transform.position, sphereRadius);
        }
#endif        
    }
}