﻿using System;
using System.Collections.Generic;
using UltimaCookie.Framework.Misc;
using UnityEngine;

namespace UltimaCookie.Framework.Components
{
    public class RendererCache : MonoBehaviour
    {
        [SerializeField]
        private MeshRenderer[] _meshRenderers;

        [SerializeField]
        private Material[] _startMaterials;
        
        private void OnEnable()
        {
            List<MeshRenderer> allRenderers = new List<MeshRenderer>();
            
            allRenderers.AddRange(transform.GetComponents<MeshRenderer>());
            allRenderers.AddRange(transform.GetComponentsInChildren<MeshRenderer>(true));

            _meshRenderers = allRenderers.ToArray();
            
            List<Material> allMaterials = new List<Material>();
            
            foreach (var renderer in allRenderers)
            {
                for (int i = 0; i < renderer.materials.Length; i++)
                {
                    allMaterials.Add(renderer.sharedMaterials[i]);
                }
            }

            _startMaterials = allMaterials.ToArray();
        }

        private void OnDestroy()
        {
            foreach (var material in _startMaterials)
            {
                Destroy(material);
            }
        }

        public void SetAllMaterialsTo(Material mat)
        {
            ToolBox.ApplyMaterialToAllSubMeshes(_meshRenderers, mat);
        }

        public void ResetAllMaterials()
        {
        }
    }
}