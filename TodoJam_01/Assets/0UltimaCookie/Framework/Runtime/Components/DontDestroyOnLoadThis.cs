﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UltimaCookie.Framework.Components
{
    public class DontDestroyOnLoadThis : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}