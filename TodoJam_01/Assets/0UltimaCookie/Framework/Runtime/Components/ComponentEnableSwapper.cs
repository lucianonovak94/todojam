using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace UltimaCookie.Framework.Components
{
    public class ComponentEnableSwapper : SerializedMonoBehaviour
    {
        [SerializeField]
        private Type _componentType;
        
        [SerializeField]
        private GameObject _other;

        private void OnEnable()
        {
            MonoBehaviour otherComp = _other.GetComponent(_componentType) as MonoBehaviour;

            if (otherComp != null)
            {
                otherComp.enabled = false;
            }
        }
        
        private void OnDisable()
        {
            MonoBehaviour otherComp = _other.GetComponent(_componentType) as MonoBehaviour;

            if (otherComp != null)
            {
                otherComp.enabled = true;
            }
        }
    }
}