
using System.Collections.Generic;

namespace UltimaCookie.Framework.Extensions
{
    public static class GeneralExtensions
    {
        // -------------  List Extensions
        public static bool AddUnique<T>(this List<T> list, T value)
        {
            if (!list.Contains(value))
            {
                list.Add(value);
                return true;
            }

            return false;
        }
        
        public static bool RemoveIfExists<T>(this List<T> list, T value)
        {
            if (list.Contains(value))
            {
                list.Remove(value);
                return true;
            }

            return false;
        }
        
        
        // -------------  String Extensions
        public static bool IsNullOrEmpty(this string val)
        {
            return string.IsNullOrEmpty(val);
        }
        
    }
}