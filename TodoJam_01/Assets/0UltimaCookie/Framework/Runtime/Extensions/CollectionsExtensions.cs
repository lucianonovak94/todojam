using System;
using System.Collections.Generic;

namespace UltimaCookie.Framework.Extensions
{
    public static class CollectionsExtensions
    {
        // -------------  Dictionary Extensions
        
        /// <summary>
        /// Delete a value if the key exists 
        /// </summary>
        public static void DeleteIfExistsKey<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key)
        {
            CheckDictionaryIsNull(dictionary);
            if (dictionary.ContainsKey(key))
                dictionary.Remove(key);
        }
        
        /// <summary>
        /// Set a Value if the key exits, otherwise add it
        /// </summary>
        public static void SetOrAddValue<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            CheckDictionaryIsNull(dictionary);
            if (!dictionary.ContainsKey(key))
                dictionary.Add(key, value);
            else
            {
                dictionary[key] = value;
            }
        }

        private static void CheckDictionaryIsNull<TKey, TValue>(this Dictionary<TKey, TValue> dictionary)
        {
            if (dictionary == null) throw new ArgumentNullException();
        }
        // -------------  end Dictionary Extensions
    }
}