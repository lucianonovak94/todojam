using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using Object = UnityEngine.Object;

/// <summary>
/// Various Extension Methods for core Unity classes
/// </summary>
namespace UltimaCookie.Framework.Extensions
{
	public static class UnityExtensions
	{
		/// <summary>
		/// Restricts value to supplied min/max
		/// </summary>
		/// <param name="value"></param>
		/// <param name="min"></param>
		/// <param name="max"></param>
		public static void Clamp(this int value, int min, int max)
		{
			if (value < min)
				value = min;

			if (value > max)
				value = max;

		}

		/// <summary>
		/// Adds or subtracts delta time to a float based on a bool
		/// </summary>
		/// <param name="timer"></param>
		/// <param name="add"></param>
		public static void IncrementClampedDeltaTime(this ref float timer, bool add)
		{
			timer = add ? Mathf.Clamp01(timer + Time.deltaTime) : Mathf.Clamp01(timer - Time.deltaTime);
		}

		/// <summary>
		/// Returns a List of Lists chunked into the specified count
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="source">The original list you want to chunk</param>
		/// <param name="chunkSize">The size of the lists that should be returned</param>
		/// <returns></returns>
		public static List<List<T>> ChunkBy<T>(this List<T> source, int chunkSize)
		{
			return source
				.Select((x, i) => new {Index = i, Value = x})
				.GroupBy(x => x.Index / chunkSize)
				.Select(x => x.Select(v => v.Value).ToList())
				.ToList();
		}

		/// <summary>
		/// Returns true if the supplied Component is a prefab, not an instance
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static bool IsPrefab(this Component obj)
		{
			return obj.gameObject.scene.rootCount == 0;
		}

		public static void RemoveComponent<T>(this Component parent) where T : Component
		{
			var component = parent.GetComponent<T>();

			if (component == null)
			{
				return;
			}

			if (Application.isPlaying)
			{
				Object.Destroy(component);
			}
			else
			{
				Object.DestroyImmediate(component, true);
			}
		}

		public static T GetComponentChecked<T>(this GameObject go) where T : Component
		{
			T comp = go.GetComponent<T>();

			if (comp == null)
			{
				Debug.LogError($"Component of type <{typeof(T)}> not found in gameobject: <{go.name}>", go);
				throw new Exception();
			}

			return comp;
		}
		
		public static T GetComponentChecked<T>(this Component c) where T : Component
		{
			T comp = c.GetComponent<T>();

			if (comp == null)
			{
				Debug.LogError($"Component of type <{typeof(T)}> not found in gameobject: <{c.name}>", c);
				throw new Exception();
			}

			return comp;
		}
		
		public static T GetComponentCheckedInParent<T>(this GameObject go) where T : Component
		{
			T comp = go.GetComponentInParent<T>();

			if (comp == null)
			{
				Debug.LogError($"Component of type <{typeof(T)}> not found in parents of gameobject: <{go.name}>", go);
				throw new Exception();
			}

			return comp;
		}
		
		public static T GetComponentCheckedInChildren<T>(this GameObject go) where T : Component
		{
			T comp = go.GetComponentInChildren<T>();

			if (comp == null)
			{
				Debug.LogError($"Component of type <{typeof(T)}> not found in children of gameobject: <{go.name}>", go);
				throw new Exception();
			}

			return comp;
		}
		

		public static T GetOrAddComponent<T>(this Component child, bool findInChildren = false) where T : Component
		{
			T result;

			if (findInChildren)
			{
				result = child.GetComponentInChildren<T>();
			}
			else
			{
				result = child.GetComponent<T>();
			}

			if (result == null)
			{
				result = child.gameObject.AddComponent<T>();
			}

			return result;
		}

		public static T GetOrAddComponent<T>(this GameObject child, bool findInChildren = false) where T : Component
		{
			T result;

			if (findInChildren)
			{
				result = child.GetComponentInChildren<T>();
			}
			else
			{
				result = child.GetComponent<T>();
			}

			if (result == null)
			{
				result = child.gameObject.AddComponent<T>();
			}

			return result;
		}

		public static GameObject GetChild(this GameObject obj, string name)
		{
			foreach (Transform transform in obj.transform)
			{
				if (transform.name == name)
				{
					return transform.gameObject;
				}
			}

			return null;
		}

		public static GameObject GetChild(this Component obj, string name)
		{
			return GetChild(obj.gameObject, name);
		}

		public static GameObject GetOrAddChild(this GameObject parent, string childName)
		{
			var child = parent.transform.Find(childName);

			if (child != null)
			{
				return child.gameObject;
			}

			var newChild = new GameObject(childName);

			newChild.transform.parent = parent.transform;

			return newChild;
		}

		public static GameObject GetOrAddChild(this Component parent, string childName)
		{
			return GetOrAddChild(parent.gameObject, childName);
		}

		public static void SetParent(this Transform target, Transform parent)
		{
			target.SetParent(parent, true);
		}

		public static void SetParent(this Component target, GameObject parent)
		{
			SetParent(target.transform, parent.transform);
		}

		public static void SetParent(this Component target, Transform parent)
		{
			SetParent(target.transform, parent);
		}

		public static void SetParent(this GameObject target, GameObject parent)
		{
			SetParent(target.transform, parent.transform);
		}

		public static void SetParent(this MonoBehaviour target, GameObject parent)
		{
			SetParent(target.transform, parent.transform);
		}

		public static void SetParent(this MonoBehaviour target, MonoBehaviour parent)
		{
			SetParent(target.transform, parent.transform);
		}

		public static void SetParent(this Camera target, MonoBehaviour parent)
		{
			SetParent(target.transform, parent.transform);
		}

		/// <summary>
		/// Update transform (world coordinates)
		/// </summary>
		/// <param name="target"></param>
		/// <param name="position"></param>
		/// <param name="rotationEuler"></param>
		public static void SetTransform(this GameObject target, Vector3 position, Vector3 rotationEuler)
		{
			SetTransform(target, position, Quaternion.Euler(rotationEuler));
		}

		/// <summary>
		/// Update transform (world coordinates)
		/// </summary>
		/// <param name="target"></param>
		/// <param name="position"></param>
		/// <param name="rotation"></param>
		public static void SetTransform(this GameObject target, Vector3 position, Quaternion rotation)
		{
			target.transform.position = position;
			target.transform.rotation = rotation;
		}

		/// <summary>
		/// Reset local transform to Vector3.zero and optionally, scale to 1
		/// </summary>
		/// <param name="target"></param>
		/// <param name="resetScale"></param>
		public static void ResetLocalTransform(this GameObject target, bool resetScale = false)
		{
			target.transform.localPosition = Vector3.zero;
			target.transform.localRotation = Quaternion.identity;

			if (resetScale)
			{
				target.transform.localScale = Vector3.one;
			}
		}

		/// <summary>
		/// Reset local transform to Vector3.zero and optionally, scale to 1
		/// </summary>
		/// <param name="target"></param>
		/// <param name="resetScale"></param>
		public static void ResetLocalTransform(this MonoBehaviour target, bool resetScale = false)
		{
			ResetLocalTransform(target.gameObject, resetScale);
		}

		/// <summary>
		/// Reset local transform to Vector3.zero and optionally, scale to 1
		/// </summary>
		/// <param name="target"></param>
		/// <param name="resetScale"></param>
		public static void ResetLocalTransform(this Component target, bool resetScale = false)
		{
			ResetLocalTransform(target.gameObject, resetScale);
		}

		/// <summary>
		/// Rotate to face the supplied <see cref="Transform"/>, optionally locking the Y Axis
		/// </summary>
		/// <param name="target"></param>
		/// <param name="transformToFace"></param>
		/// <param name="lockYRotation"></param>
		public static void FaceTransform(this Transform target, Transform transformToFace, bool lockYRotation = true)
		{
			var lookAtPos = new Vector3
			{
				x = transformToFace.position.x,
				y = (lockYRotation) ? target.position.y : transformToFace.position.y,
				z = transformToFace.position.z
			};

			target.LookAt(lookAtPos);
		}

		/// <summary>
		/// Rotate to face the supplied <see cref="Transform"/>, optionally locking the Y Axis
		/// </summary>
		/// <param name="target"></param>
		/// <param name="transformToFace"></param>
		/// <param name="lockYRotation"></param>
		public static void FaceTransform(this Component target, Transform transformToFace, bool lockYRotation = true)
		{
			FaceTransform(target.transform, transformToFace, lockYRotation);
		}

		/// <summary>
		/// Rotate to face the supplied <see cref="Transform"/>, optionally locking the Y Axis
		/// </summary>
		/// <param name="target"></param>
		/// <param name="transformToFace"></param>
		/// <param name="lockYRotation"></param>
		public static void FaceTransform(this MonoBehaviour target, Transform transformToFace,
			bool lockYRotation = true)
		{
			FaceTransform(target.transform, transformToFace, lockYRotation);
		}

		/// <summary>
		/// return the combined <see cref="Bounds"/> for all Renderers encapsulated in the supplied <see cref="GameObject"/>
		/// </summary>
		/// <param name="source"></param>
		/// <returns></returns>
		public static Bounds GetBounds(this GameObject source)
		{
			var bounds = new Bounds(Vector3.zero, Vector3.zero);

			foreach (var renderer in source.GetComponentsInChildren<Renderer>())
			{
				bounds.Encapsulate(renderer.bounds);
			}

			return bounds;
		}

		/// <summary>
		/// Discover all Renderers in the Source object and toggle visibility
		/// </summary>
		/// <param name="source"></param>
		/// <param name="visible"></param>
		public static void SetVisible(this GameObject source, bool visible)
		{
			foreach (var renderer in source.GetComponentsInChildren<Renderer>(true))
			{
				renderer.enabled = visible;
			}
		}

		public static void SetVisible(this Component source, bool visible)
		{
			SetVisible(source.gameObject, visible);
		}

		/// <summary>
		/// Centers a mesh to its Parent Transform
		/// </summary>
		/// <param name="target"></param>
		public static void CenterToParent(this MeshFilter target)
		{
			var zCenter = target.mesh.bounds.center.z;

			target.transform.localPosition = new Vector3(0, 0, 0 - zCenter);

		}

		/// <summary>
		/// Adds an OnClick event handler that accepts the clicked button as a parameter
		/// </summary>
		/// <param name="button"></param>
		/// <param name="handler"></param>
		public static void AddOnClickHandler(this Button button, UnityAction<Button> handler)
		{
			button.onClick.AddListener(delegate { handler(button); });
		}

		public static void AddOnEditHandler(this InputField input, UnityAction<InputField> handler)
		{
			input.onEndEdit.AddListener(delegate { handler(input); });
		}

		public static void AddOnToggleHander(this Toggle toggle, UnityAction<Toggle> handler)
		{
			toggle.onValueChanged.AddListener(delegate { handler(toggle); });
		}

		public static void AddOnValueChangeHandler(this Slider slider, UnityAction<Slider> handler)
		{
			slider.onValueChanged.AddListener(delegate { handler(slider); });
		}

		public static void AddOnDropdownChangeHandler(this Dropdown dropdown, UnityAction<Dropdown> handler)
		{
			dropdown.onValueChanged.AddListener(delegate { handler(dropdown); });
		}

		static Dropdown.DropdownEvent emptyDropdownEvent = new Dropdown.DropdownEvent();

		/// <summary>
		/// Changes the value of a Dropdown without triggering OnValueChanged.  To receive that event, use <see cref="Dropdown.value"/>.
		/// </summary>
		/// <param name="instance">Dropdown to change value.</param>
		/// <param name="value">Value to set toggle to.</param>
		public static void SetDropdownWithoutEvent(this Dropdown instance, int value)
		{
			var originalEvent = instance.onValueChanged;
			instance.onValueChanged = emptyDropdownEvent;
			instance.value = value;
			instance.onValueChanged = originalEvent;
		}

		static Toggle.ToggleEvent emptyToggleEvent = new Toggle.ToggleEvent();

		/// <summary>
		/// Changes the value of a UI Toggle without triggering OnValueChanged.  To receive that event, use <see cref="Toggle.isOn"/>.
		/// </summary>
		/// <param name="instance">Toggle to change value.</param>
		/// <param name="value">Value to set toggle to.</param>
		public static void SetValueWithoutEvent(this Toggle instance, bool value)
		{
			var originalEvent = instance.onValueChanged;
			instance.onValueChanged = emptyToggleEvent;
			instance.isOn = value;
			instance.onValueChanged = originalEvent;
		}
	}
}