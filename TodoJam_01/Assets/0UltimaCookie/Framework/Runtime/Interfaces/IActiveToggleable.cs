﻿using UnityEngine;

namespace UltimaCookie.Framework.Interface
{
    public interface IActiveToggleable
    {
        void DoActivate();
        void DoDeactivate();
    }
}