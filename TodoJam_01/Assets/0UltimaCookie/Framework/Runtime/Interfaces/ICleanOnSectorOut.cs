using System.Collections;

namespace UltimaCookie.Framework.Interface
{
    public interface ICleanOnSectorOut
    {
        IEnumerator Clean();
    }
}