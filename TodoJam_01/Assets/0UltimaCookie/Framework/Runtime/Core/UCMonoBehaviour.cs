﻿using UltimaCookie.Framework.Attributes;
using Sirenix.OdinInspector;
using UnityEngine;

namespace UltimaCookie.Framework.Core
{
    [SealMethodsClass]
    public abstract class UCMonoBehaviour : SerializedMonoBehaviour
    {
        protected bool canTick = true;

        private bool firstTimeRan;

        public virtual void Initialize()
        {
        }

        [SealMethod]
        private void OnEnable()
        {
            if (Application.isPlaying)
            {
                if (!firstTimeRan)
                {
                    OnEnableFirstTime();
                    firstTimeRan = true;
                }
                
                OnEnableRuntime();
            }
        }

        protected virtual void OnEnableFirstTime()  {}
        protected virtual void OnEnableRuntime()  {}
                
        [SealMethod]
        private void OnDisable()
        {
            if(Application.isPlaying)
                OnDisableRuntime();
            else
                OnDisableEditor();
        }

        protected virtual void OnDisableRuntime()  {}
        protected virtual void OnDisableEditor()  {}
        
        [SealMethod]
        private void Update()
        {
            if (canTick && Application.isPlaying)
                OnUpdate();
        }

        protected virtual void OnUpdate()
        {
        }
    }
}