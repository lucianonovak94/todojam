﻿using System;
using UltimaCookie.Framework.Events;
using UnityEngine;

namespace UltimaCookie.Framework.Core
{
    /// <summary>
    /// Provides a "smart pointer" kind of reference for values, so you can track them and check when they change
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    public class Ref<T>
    {
        [SerializeField]
        private T _value;

        private CustomEvent _onChanged = new CustomEvent();
        public CustomEvent onChanged => _onChanged; 
        
        public T Value
        {
            get { return _value; }

            set
            {
                _value = value; 
                
                onChanged.Dispatch();
            }
        }

        public Ref(T value = default(T))
        {
            _value = value;
        }
    }
}