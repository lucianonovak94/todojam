using System.Collections;
using System.Reflection;
using UltimaCookie.Framework.Attributes;
using UltimaCookie.Framework.Events;
using UnityEngine;

namespace UltimaCookie.Framework.Core
{
    [SealMethodsClass]
    public abstract class LocalSingleMonoBehaviour : MonoBehaviour
    {
        // It'll raise an error if the manager was not initialized before this time
        protected virtual float MAX_INITIALIZATION_TIME => 3f;
        
        /// <summary>
        /// If this is kept as true, the instance of this class will be automatically initialized by the
        /// corresponding <seealso cref="LocalSceneSingletons{TLocalSingle}"/> class
        /// </summary>
        public abstract bool ShouldAutoInitialize();
        
        protected bool initializationStarted = false;
        public bool InitializationStarted => initializationStarted;
        
        private bool isInitialized = false;
        public bool IsInitialized => isInitialized;

        /// <summary>
        /// IMPORTANT! Call this one in your concrete class ALWAYS after you finished initializing,
        /// otherwise the Initialization process will get stuck 
        /// </summary>
        protected void FinishInitialization() => isInitialized = true;

        /// <summary>
        /// If set to true (default), when this LocalSingleton is destroyed, it'll automatically
        /// remove all the listeners for all the EventID static and instance fields that are
        /// in the singleton
        /// </summary>
        protected virtual bool _autoRemoveAllEventIDListeners => true;

        protected bool tickIsEnabled = true;

        [SealMethod]
        public IEnumerator Initialize()
        {
            Debug.Log($"[{this.GetType().Name}] Starting Initialize");

            var timeoutRoutine = CoroutinesRunner.RunCoroutine(InitTimeoutRoutine());

            CustomInitialize();
            yield return CustomInitializeRoutine();
            
            // we need this line because we can't trust only in CustomInitialization to consider the 
            // instance initialized (the custom logic may have some async calls during the initialization 
            // process)
            yield return new WaitWhile(()=> !isInitialized );
            
            StopCoroutine(timeoutRoutine);
            
            Debug.Log($"[{this.GetType().Name}] Finished Initialize");
        }

        private IEnumerator InitTimeoutRoutine()
        {
            float timer = 0;

            while (timer < MAX_INITIALIZATION_TIME)
            {
                if (isInitialized)
                {
                    yield break;
                }

                yield return null;

                timer += Time.deltaTime;
            }
            
            if(isInitialized)
                yield break;
            
            Debug.LogError($"[{this.GetType().Name}] Failed to initialized before expected time, and was Timeouted", gameObject);
        }

        protected virtual void CustomInitialize()
        {
        }

        protected virtual IEnumerator CustomInitializeRoutine()
        {
            yield return YieldFinishInitialization();
        }

        protected IEnumerator YieldFinishInitialization()
        {
            FinishInitialization();
            yield break;
        }
        
        private void UnregisterAllEventIDListeners()
        {
            var fieldInfos = this.GetType().GetFields(
                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
            
            foreach (FieldInfo fieldInfo in fieldInfos)
            {
                if (fieldInfo.GetValue(this) is AbstractCustomEvent eventToRemove)
                {
                    eventToRemove.RemoveAllListeners();
                    
                }
            }
        }

        [SealMethod]
        private void Update()
        {
            if(isInitialized && tickIsEnabled)
                OnUpdate();
        }

        protected virtual void OnUpdate()
        {
        }

        protected virtual void OnDestroy()
        {
            if (_autoRemoveAllEventIDListeners)
            {
                UnregisterAllEventIDListeners();
            }
        }
    }
}