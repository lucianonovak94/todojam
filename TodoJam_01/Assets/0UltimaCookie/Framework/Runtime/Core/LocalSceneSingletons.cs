﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UltimaCookie.Framework.Attributes;
using Sirenix.OdinInspector;
using UnityEngine;

namespace UltimaCookie.Framework.Core
{
    public abstract class LocalSceneSingletons<TLocalSingle> : MonoBehaviour where TLocalSingle : LocalSingleMonoBehaviour
    {
        private static readonly Dictionary<Type, TLocalSingle> localSingletons = new Dictionary<Type, TLocalSingle>();
        
        private static LocalSceneSingletons<TLocalSingle> localInstance;
        protected static LocalSceneSingletons<TLocalSingle> LocalInstance => localInstance;
        
        private static Dictionary<Type, List<Action>> initializationCallbacks = new Dictionary<Type, List<Action>>();

        [Scene, SerializeField, Required]
        private string targetSceneReference;
        
        public static T Get<T>() where T : TLocalSingle
        {
            TLocalSingle instance = GetOrAdd<T>();

            if (!instance.IsInitialized)
                throw new Exception($"Trying to Get a LocalSingleton of type <{typeof(T).Name}> that was not initialized yet");

            return instance as T;
        }

        public static void NotifyWhenInitialized<T>(Action callback) where T : TLocalSingle
        {
            if (localSingletons.TryGetValue(typeof(T), out TLocalSingle instance))
            {
                if (instance.IsInitialized)
                {
                    callback.Invoke();
                    return;
                }
            }
            
            if(!initializationCallbacks.ContainsKey(typeof(T)))
                initializationCallbacks.Add(typeof(T), new List<Action>());
            
            initializationCallbacks[typeof(T)].Add(callback);
        }

        private static T GetOrAdd<T>() where T : TLocalSingle
        {
            TLocalSingle instance = null;
            
            // Most local singletons will be added at Start, but some may be created after, so we still want to be able
            // to detect them
            if (!localSingletons.TryGetValue(typeof(T), out instance))
            {
                TLocalSingle[] instances = FindObjectsOfType<T>();
                
                if(instances.Length == 0)
                    throw new Exception($"No instance Located for type {typeof(T).Name}. Be sure to add one to the Scene.");
                else if(instances.Length > 1)
                    throw new Exception($"Multiple instances Located for type {typeof(T).Name}. Be sure to set only one in the Scene.");

                instance = instances[0];
                
                localSingletons.Add(typeof(T), instance);
            }
            
            return instance as T;
        }

        private static void FindAllLocalSingleInstances()  
        {
            GameObject[] rootGameObjects = localInstance.gameObject.scene.GetRootGameObjects();

            List<TLocalSingle> allInstances = new List<TLocalSingle>();
            
            for (int i = 0; i < rootGameObjects.Length; i++)
            {
                allInstances.AddRange(rootGameObjects[i].GetComponentsInChildren<TLocalSingle>(true));
            }

            foreach (var localSingleMono in allInstances)
            {
                Type t = localSingleMono.GetType();
                if (!localSingletons.ContainsKey(t))
                {
                    localSingletons.Add(t, localSingleMono);
                }
                else
                {
                    throw new Exception($"[{t.Name}] is meant to be unique in the Scene, and multiple instances were found");
                }
            }
        }

        /// <summary>
        /// Run initialization in all the Single Instances we have in this Scene.
        /// Maybe here at start is okay and the Loading Screen only listen for a "Loading Finished" event
        /// </summary>
        public IEnumerator Initialize()
        {
            if (localInstance != null)
                throw new Exception($"[{this.GetType().Name}] is meant to be unique in the Scene");

            localInstance = this;

            if (gameObject.scene.name != targetSceneReference)
                throw new Exception($"[{this.GetType().Name}] is meant to be only in the scene called: {targetSceneReference}");

            FindAllLocalSingleInstances();
            yield return null;

            Debug.Log($"[{this.GetType().Name}] Running Automatic Singleton Inits", gameObject);
            yield return RunAutomaticInits();
            
            Debug.Log($"[{this.GetType().Name}] Running Not Automatic Singleton Inits", gameObject);
            yield return RunNotAutomaticInits();

            foreach (var kvp in localSingletons)
            {
                if (!kvp.Value.IsInitialized)
                {
                    Debug.LogError($"[{GetType().Name}] Local Singleton of type {kvp.Value.GetType().Name} was NOT initialized");
                }
            }
        }

        private void RunInitializationCallbacksForType(Type t)
        {
            if (initializationCallbacks.TryGetValue(t, out List<Action> callbacks))
            {
                foreach (Action callback in callbacks)
                {
                    callback.Invoke();
                }

                initializationCallbacks.Remove(t);
            }
        }

        private IEnumerator RunAutomaticInits()
        {
            foreach (var kvp in localSingletons)
            {
                if (kvp.Value.ShouldAutoInitialize())
                {
                    yield return YieldInitializeInstanceOfType(kvp.Key);

                    RunInitializationCallbacksForType(kvp.Key);
                }
            }
        }

        protected abstract IEnumerator RunNotAutomaticInits();

        protected IEnumerator YieldInitializeInstanceOfType(Type type)
        {
            TLocalSingle instance = localSingletons[type];
            
            if (instance.InitializationStarted)
                throw new Exception($"Trying to Initialize a LocalSingleton of type <{type.Name}> that already started or finished its initialization");

            yield return instance.Initialize();
            
            RunInitializationCallbacksForType(type);
        }

        protected IEnumerator YieldInitializeInstanceOfType<T>() where T : TLocalSingle
        {
            TLocalSingle instance = GetOrAdd<T>();
            
            if (instance.InitializationStarted)
                throw new Exception($"Trying to Initialize a LocalSingleton of type <{typeof(T).Name}> that already started or finished its initialization");

            yield return instance.Initialize();
            
            RunInitializationCallbacksForType(typeof(T));
        }
        
        private void OnDestroy()
        {
            localInstance = null;
            localSingletons.Clear();
        }
    }
}