﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace UltimaCookie.Framework.Core
{
    public abstract class SerializedScriptableSingleton<T> : SerializedScriptableObject where T : ScriptableObject
    {
        static T _instance = null;

        public static T Get()
        {
            if (_instance == null)
            {
                var instances = Resources.LoadAll<T>("");
                _instance = instances[0];
            }
                
            return _instance;
        }
    }
}