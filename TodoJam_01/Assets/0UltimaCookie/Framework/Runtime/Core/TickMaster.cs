﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UltimaCookie.Framework.Core
{
    public enum ETickUpdateMoment
    {
        Update, FixedUpdate, LateUpdate
    }

    public enum EStackMethod
    {
        UseLastAdded, IgnoreSubsequents
    }
    
    /// <summary>
    /// Tick Master provides a way to register and unregister functions to the Update scopes, letting in this way
    /// activate/deactivate per frame logic around the code base (avoiding then to have potentially multiple Monobehaviours
    /// running the Update but doing nothing because a condition is not met).
    ///
    /// Since it needs to be a MonoBehavior so it runs the Updates, it extends from CoreBehaviorSingleton,
    /// however, no instance public access is required, and all the interface and variable are static.
    ///
    /// You can setup framerate for actions. If so, the delta time will be adjusted to calculated the delta between
    /// invocations
    ///
    /// TODO:
    /// create some functional testing for this would be ideal
    ///
    /// TODO features:
    /// - Auto Check if some Action is null because it was destroyed and not unregistered?
    /// - Create an Destroy/Disable Notificator for MonoBehaviours so we can "auto" disable actions?
    /// </summary>
    public class TickMaster : MonoBehaviour
    {
        /// <summary>
        /// Wraps a callback to be executed in custom tick timing.
        /// The timing can be frame rated (using CurrentFrame/FrameRate) or timed (using TimeInterval > DeltaTime)
        /// </summary>
        private class TickFrameRatedAction
        {
            public Action<float> function;
            public short currentFrame;
            public short frameRate;
            public float lastInvokeTime;
            public float timeInterval;
            public bool markedForRemove;
        }

        private static readonly Dictionary<ETickUpdateMoment, HashSet<Action<float>>> actionsByMomentDefault =
            new Dictionary<ETickUpdateMoment, HashSet<Action<float>>>
            {
                {ETickUpdateMoment.Update, new HashSet<Action<float>>()},
                {ETickUpdateMoment.FixedUpdate, new HashSet<Action<float>>()},
                {ETickUpdateMoment.LateUpdate, new HashSet<Action<float>>()}
            };

        private static readonly Dictionary<ETickUpdateMoment, HashSet<TickFrameRatedAction>> actionsByMomentFrameRated =
            new Dictionary<ETickUpdateMoment, HashSet<TickFrameRatedAction>>
            {
                {ETickUpdateMoment.Update, new HashSet<TickFrameRatedAction>()},
                {ETickUpdateMoment.FixedUpdate, new HashSet<TickFrameRatedAction>()},
                {ETickUpdateMoment.LateUpdate, new HashSet<TickFrameRatedAction>()}
            };
        
        private static readonly Dictionary<ETickUpdateMoment, HashSet<TickFrameRatedAction>> actionsByMomentTimed =
            new Dictionary<ETickUpdateMoment, HashSet<TickFrameRatedAction>>
            {
                {ETickUpdateMoment.Update, new HashSet<TickFrameRatedAction>()},
                {ETickUpdateMoment.FixedUpdate, new HashSet<TickFrameRatedAction>()},
                {ETickUpdateMoment.LateUpdate, new HashSet<TickFrameRatedAction>()}
            };

        // using hashcode instead of the actual action
        private static readonly Dictionary<int, ETickUpdateMoment> tickUpdateMomentForActions = new Dictionary<int, ETickUpdateMoment>();

        private static readonly Dictionary<int, TickFrameRatedAction> actionsWithFrameRate =
            new Dictionary<int, TickFrameRatedAction>();
        
        private static readonly List<TickFrameRatedAction> framedActionsToRemove = new List<TickFrameRatedAction>();
        
        private static readonly Dictionary<int, TickFrameRatedAction> actionsWithTimeRate =
            new Dictionary<int, TickFrameRatedAction>();
        
        private static readonly List<TickFrameRatedAction> timedActionsToRemove = new List<TickFrameRatedAction>();

        private static readonly Dictionary<ETickUpdateMoment, Dictionary<int, TickFrameRatedAction>> executeOnceActions
            = new Dictionary<ETickUpdateMoment, Dictionary<int, TickFrameRatedAction>>
            {
                {ETickUpdateMoment.Update, new Dictionary<int, TickFrameRatedAction>()},
                {ETickUpdateMoment.FixedUpdate, new Dictionary<int, TickFrameRatedAction>()},
                {ETickUpdateMoment.LateUpdate, new Dictionary<int, TickFrameRatedAction>()}
            };
        private static readonly List<int> executeOnceToRemove = new List<int>();

        [RuntimeInitializeOnLoadMethod]
        private static void Initialize()
        {
            GameObject go = new GameObject();
            go.AddComponent<TickMaster>();
        }
        
        /// <summary>
        /// </summary>
        /// <param name="stackMethod">
        /// UseLastAdded: if the action is re added before it's executed, the frames value will be updated the last value
        /// This means that the time to execute will be updated and pushed forward.
        /// IgnoreSubsequents: if the action is re added before it's executed, the new value will be ignored and the action
        /// still will be executed in the expected schedule
        /// </param>
        public static void ExecuteOnceAfterFrames(Action<float> action, ETickUpdateMoment updateMoment,
            EStackMethod stackMethod, short afterFrames)
        {
            TickFrameRatedAction actionObject = null;
            if (!executeOnceActions[updateMoment].TryGetValue(action.GetHashCode(), out actionObject))
            {
                actionObject = new TickFrameRatedAction()
                {
                        function = action,
                        currentFrame = 0,
                        lastInvokeTime = Time.time, 
                        frameRate = afterFrames
                };
                
                executeOnceActions[updateMoment].Add(action.GetHashCode(), actionObject);
            }
            else
            {
                if (stackMethod == EStackMethod.UseLastAdded)
                {
                    actionObject.currentFrame = 0;
                    actionObject.frameRate = afterFrames;
                }
            }
        }

        public static void ExecuteOnceAfterTime(Action action, float delay)
        {
            CoroutinesRunner.RunCoroutine(ExecuteOnceRoutine(action, delay));
        }

        private static IEnumerator ExecuteOnceRoutine(Action action, float delay)
        {
            yield return new WaitForSeconds(delay);
            action.Invoke();
        }

        public static void AddAction(Action<float> action, ETickUpdateMoment updateMoment = ETickUpdateMoment.Update, short frameRate = 1)
        {
            int actionHashCode = action.GetHashCode();

            if(tickUpdateMomentForActions.ContainsKey(actionHashCode))
                return;

            tickUpdateMomentForActions.Add(actionHashCode, updateMoment);

            if (frameRate <= 1)
            {
                actionsByMomentDefault[updateMoment].Add(action);
            }
            else
            {
                var tickRatedAction = new TickFrameRatedAction()
                {
                    function = action,
                    currentFrame = frameRate,
                    frameRate = frameRate,
                    lastInvokeTime = Time.time 
                };

                actionsByMomentFrameRated[updateMoment].Add(tickRatedAction);
                actionsWithFrameRate.Add(actionHashCode, tickRatedAction);
            }
        }

        public static void AddTimedAction(Action<float> action, float secondsInterval, ETickUpdateMoment updateMoment = ETickUpdateMoment.Update, bool executeWhenAdded = false)
        {
            int actionHashCode = action.GetHashCode();

            if(actionsWithTimeRate.ContainsKey(actionHashCode))
                return;

            tickUpdateMomentForActions.Add(actionHashCode, updateMoment);

            var tickRatedAction = new TickFrameRatedAction()
            {
                function = action,
                currentFrame = 0,
                frameRate = 0,
                lastInvokeTime = Time.time,
                timeInterval = secondsInterval
            };

            actionsByMomentTimed[updateMoment].Add(tickRatedAction);
            actionsWithTimeRate.Add(actionHashCode, tickRatedAction);

            if (executeWhenAdded)
            {
                action.Invoke(0);
            }
        }
        
        public static void RemoveAction(Action<float> action)
        {
            int actionHashCode = action.GetHashCode();

            if(!tickUpdateMomentForActions.TryGetValue(actionHashCode, out var updateMoment))
                return;

            tickUpdateMomentForActions.Remove(actionHashCode);

            if (actionsWithFrameRate.ContainsKey(actionHashCode))
            {
                actionsWithFrameRate[actionHashCode].markedForRemove = true;
            }
            else if (actionsWithTimeRate.ContainsKey(actionHashCode))
            {
                actionsWithTimeRate[actionHashCode].markedForRemove = true;
            }
            else
            {
                actionsByMomentDefault[updateMoment].Remove(action);    
            }
        }

        private void Update()
        {
            TickMoment(ETickUpdateMoment.Update);
        }

        private void FixedUpdate()
        {
            TickMoment(ETickUpdateMoment.FixedUpdate);
        }

        private void LateUpdate()
        {
            TickMoment(ETickUpdateMoment.LateUpdate);
        }

        private void TickMoment(ETickUpdateMoment moment)
        {
            var enumeratorDefault = actionsByMomentDefault[moment].GetEnumerator();

            // Do all the default ticks (each frame tick)
            while (enumeratorDefault.MoveNext())
            {
                enumeratorDefault.Current.Invoke(Time.deltaTime);
            }

            // Do the tick for all the FrameRated Actions
            var enumeratorFrameRated = actionsByMomentFrameRated[moment].GetEnumerator();

            while (enumeratorFrameRated.MoveNext())
            {
                TickFrameRatedAction framedAction = enumeratorFrameRated.Current;

                if (framedAction.markedForRemove)
                {
                    framedActionsToRemove.Add(framedAction);
                }
                else
                {
                    if (framedAction.currentFrame >= framedAction.frameRate)
                    {
                        framedAction.function.Invoke(Time.time - framedAction.lastInvokeTime);
                        framedAction.currentFrame = 0;
                        framedAction.lastInvokeTime = Time.time;
                    }
                    else
                    {
                        framedAction.currentFrame++;    
                    }
                }
            }

            // Do the tick for all the TimeInterval Actions
            var enumeratorTimeRated = actionsByMomentTimed[moment].GetEnumerator();

            while (enumeratorTimeRated.MoveNext())
            {
                TickFrameRatedAction timedAction = enumeratorTimeRated.Current;
                
                if (timedAction.markedForRemove)
                {
                    timedActionsToRemove.Add(timedAction);
                }
                else
                {
                    if ( timedAction.timeInterval <= Time.time - timedAction.lastInvokeTime)
                    {
                        timedAction.function.Invoke(Time.time - timedAction.lastInvokeTime);
                        timedAction.lastInvokeTime = Time.time;
                    }
                }
            }

            
            // these two for could be placed in a method, but I'm leaving them here to avoid another stack call
            for(int i = framedActionsToRemove.Count -1; i >= 0; i--)
            {
                actionsByMomentFrameRated[moment].Remove(framedActionsToRemove[i]);
                actionsWithFrameRate.Remove(framedActionsToRemove[i].GetHashCode());
            }
            framedActionsToRemove.Clear();
            
            for(int i = timedActionsToRemove.Count -1; i >= 0; i--)
            {
                actionsByMomentTimed[moment].Remove(timedActionsToRemove[i]);
                actionsWithTimeRate.Remove(timedActionsToRemove[i].GetHashCode());
            }
            timedActionsToRemove.Clear();

            /// Execute once methods
            var executeOnceEnumerator = executeOnceActions[moment].Values.GetEnumerator();

            while (executeOnceEnumerator.MoveNext())
            {
                if (executeOnceEnumerator.Current.currentFrame >= executeOnceEnumerator.Current.frameRate)
                {
                    executeOnceEnumerator.Current.function.Invoke(Time.time - executeOnceEnumerator.Current.lastInvokeTime);
                    executeOnceEnumerator.Current.markedForRemove = true;
                    executeOnceToRemove.Add(executeOnceEnumerator.Current.function.GetHashCode());
                }
                else
                {
                    executeOnceEnumerator.Current.currentFrame++;
                }
            }

            foreach (int hashToRemove in executeOnceToRemove)
            {
                executeOnceActions[moment].Remove(hashToRemove);
            }
            executeOnceToRemove.Clear();
        }
    }
}