using System.Collections;
using UnityEngine;

namespace UltimaCookie.Framework.Core
{
    public class CoroutinesRunner : UCMonoBehaviour
    {
        private static CoroutinesRunner _instance;
        
        [RuntimeInitializeOnLoadMethod]
        private static void CreateInstance()
        {
            GameObject go = new GameObject("CoroutinesRunner");
            _instance = go.AddComponent<CoroutinesRunner>();   
            DontDestroyOnLoad(go);
        }

        public static Coroutine RunCoroutine(IEnumerator enumerator)
        {
            return _instance.StartCoroutine(enumerator);
        }
        
        public static void KillCoroutine(Coroutine routine)
        {
            _instance.StopCoroutine(routine);
        }
        
        public static void KillCoroutine(IEnumerator enumerator)
        {
            _instance.StopCoroutine(enumerator);
        }
    }
}