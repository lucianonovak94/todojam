﻿
namespace UltimaCookie.Framework.Core
{
    public abstract class SimpleSingleton<T> where T : class, new ()
    {
        private static readonly T instance = new T();

        public static T Get() => instance;
    }
}