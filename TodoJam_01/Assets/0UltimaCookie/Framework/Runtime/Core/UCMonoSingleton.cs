﻿using UltimaCookie.Framework.Components;
using UltimaCookie.Framework.Extensions;
using UltimaCookie.Framework.Attributes;
using UnityEngine;
using UnityEngine.Events;

namespace UltimaCookie.Framework.Core
{
    public class UCMonoSingleton<T> : UCMonoBehaviour where T : MonoBehaviour
    {
        private static readonly UnityEvent onInitialized = new UnityEvent();
        
        protected static T instance;

        // override this if your singleton should persist between scenes
        protected virtual bool _persistBetweenScenes => false;

        private static bool _initialized = false;
        
        public static void NotifyWhenInitialized(UnityAction callback)
        {
            if (_initialized)
            {
                callback.Invoke();
            }
            else
            {
                onInitialized.AddListener(callback);
            }
        }

        public static T Get()
        {
            if (instance == null)
            {
                T[] allInstance = FindObjectsOfType<T>();

                if (allInstance.Length == 0)
                {
                    Debug.LogError($"No instance found for type <{typeof(T).Name}>");     
                    return null;
                }

                if (allInstance.Length > 1)
                {
                    Debug.LogError($"Singleton multiple instances found of type: {typeof(T)}");
                }

                instance = allInstance[0];

                if ((instance as UCMonoSingleton<T>)._persistBetweenScenes)
                {
                    instance.GetOrAddComponent<DontDestroyOnLoadThis>();
                }

                if (instance == null)
                {
                    Debug.LogError($"No instance found for type <{typeof(T).Name}>");                                                            
                }
            }

            return instance;
        }

        [SealMethod]
        public override void Initialize()
        {
            if (_initialized)
            {
                Debug.LogError($"Trying to initialize an instance of Singleton <{this.GetType().Name}> that's already initialized.");
                return;
            }

            CustomInitialize();
            onInitialized.Invoke();
            onInitialized.RemoveAllListeners();
            _initialized = true;
        }

        protected virtual void CustomInitialize()
        {
        }

        [SealMethod]
        private void OnDestroy()
        {
            _initialized = false;
            DoOnDestroyed();
        }

        protected virtual void DoOnDestroyed()
        {
        }
    }
}