﻿using UltimaCookie.Framework.Core;

public class UIPanelPayloaded<T> : UIPanel where T : UIPanelPayload
{
    protected T openPayload;
    
    public void Show(T payload)
    {
        openPayload = payload;
        
        BeforeShow();
        gameObject.SetActive(true);
    }

}

public abstract class UIPanelPayload
{
}
