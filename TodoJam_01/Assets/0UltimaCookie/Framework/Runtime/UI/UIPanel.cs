﻿using System;
using UltimaCookie.Framework.Core;
using UltimaCookie.Framework.Events;

public class UIPanel : UCMonoBehaviour
{
    [NonSerialized]
    public CustomEvent onOpened = new CustomEvent();
    
    [NonSerialized]
    public CustomEvent onClosed = new CustomEvent();
    
    public void Show()
    {
        BeforeShow();
        gameObject.SetActive(true);
        onOpened.Dispatch();
        AfterShow();
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        AfterHide();
        onClosed.Dispatch();
    }

    protected virtual void BeforeShow()
    {
    }

    protected virtual void AfterShow()
    {
    }

    protected virtual void AfterHide()
    {
    }
}
