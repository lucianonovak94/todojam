﻿using UltimaCookie.Framework.Core;

public abstract class UITabLogic : UCMonoBehaviour
{
    private void Awake()
    {
        canTick = false;
    }

    public abstract void BeforeShow();

    public abstract void AfterHide();
}
