﻿using DG.Tweening;
using Sirenix.OdinInspector;
using UltimaCookie.Framework.Core;
using UltimaCookie.Framework.Events;
using UltimaCookie.Framework.Extensions;
using UnityEngine;

public class UITab : UCMonoBehaviour
{
    public static readonly CustomEvent onAnyShown = new CustomEvent();
    public static readonly CustomEvent onAllHidden = new CustomEvent();
    
    private float closedY = -3000f;

    [SerializeField, Required]
    private TabType _tabType;
    public TabType GetTabType() => _tabType;

    private RectTransform _rectTransform;

    private UITabLogic _logic;
    
    private void Awake()
    {
        _logic = gameObject.GetComponentChecked<UITabLogic>();
        _rectTransform = GetComponent<RectTransform>();
        canTick = false;
        Hide(false);
    }

    public override void Initialize()
    {
        _logic.Initialize();
    }

    public void Show(bool useTransition)
    {
        _logic.BeforeShow();
        
        _rectTransform.anchoredPosition = new Vector2(0, closedY); 
        gameObject.SetActive(true);

        if (useTransition)
        {
            _rectTransform.DOAnchorPosY(0, 0.2f)
                .OnComplete(() =>
                {
                    onAnyShown.Dispatch();
                });
        }
        else
        {
            _rectTransform.anchoredPosition = new Vector2(0, 0);    
        }
    }

    public void Hide(bool useTransition)
    {
        if (useTransition)
        {
            onAllHidden.Dispatch();
            _rectTransform.DOAnchorPosY(closedY, 0.2f).OnComplete(() =>
            {
                gameObject.SetActive(false);        
            });
        }
        else
        {
            _rectTransform.anchoredPosition = new Vector2(closedY, 0);
            gameObject.SetActive(false);
        }
        
        _logic.AfterHide();
    }
}
