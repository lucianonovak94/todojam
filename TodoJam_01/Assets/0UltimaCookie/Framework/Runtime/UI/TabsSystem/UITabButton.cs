﻿using Sirenix.OdinInspector;
using UltimaCookie.Framework.Extensions;
using UnityEngine;
using UnityEngine.UI;

public class UITabButton : MonoBehaviour
{
    [Required]
    public TabType tabType;

    private Button _button;
    
    private void Awake()
    {
        _button = gameObject.GetComponentChecked<Button>();
        _button.onClick.AddListener(() =>
        {
            UITabManager.Get().OpenTab(tabType);
        });
    }
}
