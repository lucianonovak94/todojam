﻿using UnityEngine;
using UnityEngine.UI;

public class UICloseTabButton : MonoBehaviour
{
    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(UITabManager.Get().CloseCurrentTab);
    }
}
