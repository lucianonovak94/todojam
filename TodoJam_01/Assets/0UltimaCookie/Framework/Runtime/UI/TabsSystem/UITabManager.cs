﻿using System.Collections.Generic;
using UltimaCookie.Framework.Core;
using UnityEngine;

public class UITabManager : UCMonoSingleton<UITabManager>
{
    [SerializeField]
    private Dictionary<TabType, UITab> _tabsByTypes = new Dictionary<TabType, UITab>();

    private UITab _currentOpenedTab = null;
    
    protected override void CustomInitialize()
    {
        UITab[] allTabs = transform.GetComponentsInChildren<UITab>(true);

        foreach (var tab in allTabs)
        {
            tab.Initialize();
            _tabsByTypes.Add(tab.GetTabType(), tab);
        }
    }

    public void OpenTab(TabType type)
    {
        if (_currentOpenedTab != null && _currentOpenedTab.GetTabType() == type)
        {
            CloseCurrentTab();
            return;
        }

        if (_currentOpenedTab != null)
        {
            _currentOpenedTab.Hide(false);

            _currentOpenedTab = _tabsByTypes[type];
            _currentOpenedTab.Show(false);
        }
        else
        {
            _currentOpenedTab = _tabsByTypes[type];
            _currentOpenedTab.Show(true);   
        }
    }

    public void CloseCurrentTab()
    {
        _currentOpenedTab.Hide(true);
        _currentOpenedTab = null;
    }
}
