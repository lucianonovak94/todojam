﻿using System;
using System.IO;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using UC.Managers;
using UnityEditor;
using UnityEngine;

public class EditorGeneralOptionsSObj
{
    public GeneralOptionsData data;
    
    public EditorGeneralOptionsSObj()
    {
        try
        {
            Load();
        }
        catch (Exception e)
        {
        }
    }

    private void Load()
    {
        string dataString = File.ReadAllText(UCConfigs.configFilePath());
        data = JsonConvert.DeserializeObject<GeneralOptionsData>(dataString);
    }

    [Button("Config Project")]
    private void OnClick_ConfigProject()
    {
        try
        {
            Directory.CreateDirectory(UCConfigs.generatedPath());
            Directory.CreateDirectory(UCConfigs.generatedPath() + "/Resources");
            AssetDatabase.Refresh();
        }
        catch (Exception ex)
        {
        }
        
        // -- config file
        if (!File.Exists(UCConfigs.configFilePath()))
        {
            data = new GeneralOptionsData();
            data.Save();
        }
        
        Load();
    }
}
