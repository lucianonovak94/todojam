﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

[InitializeOnLoad]
public class AutoRunScene
{
    static AutoRunScene()
    {
        EditorApplication.playModeStateChanged += OnPlayModeChanged;
        EditorApplication.quitting += OnExitingApplication;
    }

    private static void OnExitingApplication()
    {
        EditorPrefs.DeleteKey("PrePlayScenes");
    }

    private static void OnPlayModeChanged(PlayModeStateChange state)
    {
        if (!EditorPrefs.HasKey("AutoRunScene"))
        {
            return;
        }

        if (state == PlayModeStateChange.ExitingEditMode)
        {
            DoBootCleanse();
        }
        else if (state == PlayModeStateChange.EnteredEditMode)
        {
            RestoreScenes();
        }
    }

    private static void RestoreScenes()
    {
        string storedScenes = EditorPrefs.GetString("PrePlayScenes");
        
        string[] scenesToRestore = storedScenes.Split(new char[] {'|'}, StringSplitOptions.RemoveEmptyEntries);
        
        List<Scene> openingScenes = new List<Scene>();
        
        for (int i = scenesToRestore.Length - 1; i >= 0; i--)
        {
            Scene sceneToRestore = EditorSceneManager.OpenScene(scenesToRestore[i]);
            EditorSceneManager.OpenScene(scenesToRestore[i]);
            
            openingScenes.Add(sceneToRestore);
        }
        
        for (int i = 0; i < EditorSceneManager.sceneCount; i++)
        {
            Scene scene = EditorSceneManager.GetSceneAt(i);

            if (openingScenes.Contains(scene))
            {
                EditorSceneManager.OpenScene(scene.path, OpenSceneMode.Additive);
            }
            else
            {
                EditorSceneManager.CloseScene(scene, true);
            }
        }
        
        
        
        EditorPrefs.DeleteKey("PrePlayScenes");
    }

    private static void DoBootCleanse()
    {
        EditorSceneManager.SaveOpenScenes();
        
        List<Scene> scenesToUnload = new List<Scene>();

        string allSceneNames = "";

        string sceneName = EditorPrefs.GetString("AutoRunScene");
        
        bool bootIsThere = false;
        
        for (int i = 0; i < EditorSceneManager.sceneCount; i++)
        {
            Scene sceneToClean = EditorSceneManager.GetSceneAt(i);

            if (sceneToClean.name != sceneName)
                scenesToUnload.Add(sceneToClean);
            else
                bootIsThere = true;
        }

        if (!bootIsThere)
        {
            EditorSceneManager.OpenScene($"Assets/0UltimaCookie/Framework/Scenes/{sceneName}.unity", OpenSceneMode.Additive);
        }

        for (int i = scenesToUnload.Count - 1; i >= 0; i--)
        {
            Scene sceneToClean = scenesToUnload[0];

            allSceneNames += sceneToClean.path + "|";
            
            EditorSceneManager.CloseScene(sceneToClean, false);
            
            EditorPrefs.SetString("SceneToStart", sceneToClean.name);
        }
        
         
        EditorPrefs.SetString("PrePlayScenes", allSceneNames);
    }
}
