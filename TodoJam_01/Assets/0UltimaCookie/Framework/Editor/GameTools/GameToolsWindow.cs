﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using ToolWindowClasses;
using Sirenix.OdinInspector.Editor;
using UltimaCookie.Framework.Editor;
using UnityEditor;
using UnityEngine;

namespace UC.MomoRacers.Game
{
    [Serializable]
    public class ScenesAccessor
    {
        [ListDrawerSettings(Expanded = true)]
        public List<SceneAsset> scenes = new List<SceneAsset>();

        public ScenesAccessor()
        {
            scenes = EditorToolBox.GetAllAssetsOfType<SceneAsset>("Assets/0Game/Scenes");
        }
    }

    public class GameToolsWindow : OdinMenuEditorWindow
    {
        private static GameToolsWindow instance;
        
        [MenuItem("UltimaCookie/Tools Window")]
        public static void OpenWindow()
        {
            instance = GetWindow<GameToolsWindow>();
            instance.Show();
        }

        protected override OdinMenuTree BuildMenuTree()
        {
            OdinMenuTree tree = new OdinMenuTree();
            tree.Selection.SupportsMultiSelect = true;
            tree.Selection.SelectionChanged += OnSelectionChanged;
            
            tree.AddMenuItemAtPath("", new OdinMenuItem(tree, "Scenes", new ScenesAccessor()));
            
            tree.AddMenuItemAtPath("", new OdinMenuItem(tree, "General", new EditorGeneralOptionsSObj()));
            
            tree.AddMenuItemAtPath("", new OdinMenuItem(tree, "UI Prefabs", new UIPrefabsConfig()));

            tree.AddAssetAtPath("Debug", "Assets/0Game/0Data/DebugOptions.asset");

            tree.AddAssetAtPath("Chat Config", "Assets/0Data/Resources/ChatConfig.asset");

            return tree;
        }

        private void OnSelectionChanged(SelectionChangedType obj)
        {
        }
        
        
    }
}
