﻿using UnityEngine.SceneManagement;

public class AutoReloader
{
    [UnityEditor.Callbacks.DidReloadScripts]
    private static void OnScriptsReloaded()
    {
        if (UnityEditor.EditorApplication.isPlaying)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
