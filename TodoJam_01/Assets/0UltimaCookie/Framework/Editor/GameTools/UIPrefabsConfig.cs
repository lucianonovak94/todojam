using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace ToolWindowClasses
{
    public abstract class AUIPrefabsConfig
    {
        protected virtual void LoadCustomReferences()
        {
            
        }
    }

    [System.Serializable]
    public partial class UIPrefabsConfig : AUIPrefabsConfig
    {
        [SerializeField]
        [BoxGroup("Main Canvas")]
        private GameObject _mainCanvas;
        
        public UIPrefabsConfig()
        {
            _mainCanvas = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/0Game/Prefabs/UI/UI.prefab");
            
            LoadCustomReferences();
        }
    }
}