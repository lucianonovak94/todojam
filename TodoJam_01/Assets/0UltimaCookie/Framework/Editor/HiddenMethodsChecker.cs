﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UltimaCookie.Framework.Attributes;
using UnityEditor;
using UnityEngine;

namespace UltimaCookie.Framework.Editor
{
    public static class HiddenMethodsChecker
    {

        [InitializeOnLoadMethod]
        private static void CheckClasses()
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies)
            {
                foreach (var type in assembly.GetTypes())
                {
                    SealMethodsClassAttribute atts = type.GetCustomAttribute<SealMethodsClassAttribute>();

                    if (atts != null)
                    {
                        CheckClass(type);
                    }
                }
            }
        }
        
        private static void CheckClass(Type t)
        {
            MethodInfo[] methodInfos = t.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            methodInfos = methodInfos.Where(m => m.GetCustomAttributes(typeof(SealMethodAttribute)).Any()).ToArray();
            
            List<Type> subclassesOfT =
                (from assembly in AppDomain.CurrentDomain.GetAssemblies()
                from type in assembly.GetTypes()
                where type.IsSubclassOf(t)
                select type).ToList();
            
            for (var i = 0; i < methodInfos.Length; i++)
            {
                foreach (Type subClass in subclassesOfT)
                {
                    MethodInfo[] existentMethods = subClass.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).Where(methodInfo => methodInfo.Name == methodInfos[i].Name).ToArray();

                    foreach (var method in existentMethods)
                    {
                        if (method.MethodHandle != methodInfos[i].MethodHandle)
                        {
                            Debug.LogError($"Error in Subclass <{subClass}>, it's overriding/hiding the method <{method.Name}> from class <{t}> and should NOT be doing this.");
                        }
                    }
                }
            }
        }
    }
}